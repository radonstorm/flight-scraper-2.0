# pushbullet.py - send a message to a pushbullet user
# Author - Jack Lardner
# Last modified 10/12/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# used to notify me when something has gone wrong

import requests
import sys
import json
import general.pushbullet_config as pushbullet_config
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


def send_notification(message, title='Flight Scraper Notification'):
    pushbullet_token = pushbullet_config.TOKEN
    push = requests.post(
        'https://api.pushbullet.com/v2/pushes',
        headers={
            'Access-Token': pushbullet_token,
            'Content-Type': 'application/json'
        },
        data=json.dumps({
            'title': title,
            'body': message,
            'type': 'note'
        })
    )
    return push


if __name__ == '__main__':
    if len(sys.argv) > 1:
        send_notification(sys.argv[1])
    else:
        # here we have what the cron job should run,
        # we see how many scrapes there are per scraper and if one is missing
        # send a notification
        mw = Flightdata_Middleware()
        send = False
        message = 'The following scrapers have not reported any airfare data: '
        scrapers = [
            # 'Jetstar',
            'AirNorth',
            'Virgin',
            'Skippers',
            'Qantas'
        ]
        for scraper in scrapers:
            numairfares = mw.execute_sql(f"SELECT NumAirfares('{scraper}')")[0]
            if numairfares == 0:
                send = True
                message += f'\n{scraper}'
        num_google_airfares = mw.execute_sql("SELECT NumGoogleAirfares()")[0]
        if num_google_airfares == 0:
            send = True
            message += '\nGoogle Flights'
        if send:
            send_notification(message)
        mw.close_connection()
