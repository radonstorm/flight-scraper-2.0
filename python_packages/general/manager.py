# manager.py - Contains base class for all scraper managers
# Author - Jack Lardner
# Last modified - 21/10/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from scrapy.utils.project import get_project_settings
from crochet import run_in_reactor
from datetime import datetime
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
import time


class Manager:
    def __init__(self, scraper_name, scraper_settings, setting_query):
        self.scraper_name = scraper_name
        self.scraper_settings = scraper_settings
        self.wait_time = 60
        mw = Flightdata_Middleware()
        startdate_setting = mw.get_setting(setting_query, datetime.utcnow())
        startdate_setting = startdate_setting.split('-')
        self.start_date = datetime(year=int(startdate_setting[0]), month=int(startdate_setting[1]), day=int(startdate_setting[2]))
        mw.close_connection()

    def start(self, dates, route):
        print(f'scraping {route}')
        for date in dates:
            try:
                self.run(route.split('-')[0], route.split('-')[1], date).wait(9999)
                time.sleep(self.wait_time)
                self.run(route.split('-')[1], route.split('-')[0], date).wait(9999)
                time.sleep(self.wait_time)
            except Exception as e:
                print(e)
        # sleep between each route, but also sleep between each scrape
        time.sleep(self.wait_time)

    @run_in_reactor
    def run(self, dep, dest, date):
        """abstract run method that must be implemented in subclass
        scraper running will differ between scrapers
        """
        raise NotImplementedError('run has to be implemented, as running each scraper is unique')
