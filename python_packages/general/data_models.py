"""
Classes for data prodced by the scraper
"""


class ModelDict:
    reqFields = []
    optFields = []

    @classmethod
    def createDict(cls, **kwargs):
        """
        Creates a dict for the data, checking that all required fields are present.
        Raises an error if any required fields are missing.

        Fields must be passed in as keyword arguments.
        """
        data = {}
        for req in cls.reqFields:
            if req in kwargs:
                data[req] = kwargs[req]
            else:
                raise Exception("Missing required field: " + req)
        for opt in cls.optFields:
            if opt in kwargs:
                data[opt] = kwargs[opt]
        return data


class ScraperSession(ModelDict):
    reqFields = [
        "sessionStart",
        "sessionEnd",
        "targetSite",
        "errors",
        "scraperIP"
    ]


class ScraperLog(ModelDict):
    reqFields = [
        "sessionStart",
        "targetSite",
        "timestamp",
        "targetURL",
        "flightNumber",
        "dep_time",
        "arr_time",
        "data",
        "status"
    ]
    optFields = [
        "message"
    ]


class ScraperError(ModelDict):
    reqFields = [
        "timestamp",
        "targetURL",
        "severity",
        "errorDetails",
    ]
