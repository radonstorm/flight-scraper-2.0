# airport_data.py - Contains data structures for common information on airports
# Author - Jack Lardner
# Last Modified - 23/09/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

import csv
import os


# AirportData - Contains IATA codes, latitudes and longitudes for a given airport
class AirportData:
    # codes gathered from https://www.iata.org/publications/pages/code-search.aspx (accessed 5/10/19)
    iata_codes = {
        'perth': 'PER',
        'sydney': 'SYD',
        'melbourne': 'MEL',
        'adelaide': 'ADL',
        'brisbane': 'BNE',
        'cairns': 'CNS',
        'karratha': 'KTA',
        'kalgoorlie': 'KGI',
        'port_hedland': 'PHE',
        'newman': 'ZNE',
        'geraldton': 'GET',
        'broome': 'BME',
        'kununurra': 'KNX',
        'learmonth': 'LEA',
        'carnarvon': 'CVQ',
        'esperance': 'EPR',
        'albany': 'ALH',
        'shark_bay': 'MJK',
        'mount_magnet': 'MMG',
        'meekatharra': 'MKR',
        'woodie_woodie': 'WWI',
        'west_angelas': 'WLP',
        'ginbata': 'GBW',
        'christmas_creek': 'CKW',
        'granites': 'GTS'
    }

    # airport name data taken from https://openflights.org/html/apsearch (accessed 7/10/19)
    airport_names = {
        'perth': 'Perth International Airport',
        'sydney': 'Sydney Kingsford Smith International Airport',
        'melbourne': 'Melbourne International Airport',
        'adelaide': 'Adelaide International Airport',
        'brisbane': 'Brisbane International Airport',
        'cairns': 'Cairns International Airport',
        'karratha': 'Karratha Airport',
        'kalgoorlie': 'Kalgoorlie Boulder Airport',
        'port_hedland': 'Port Hedland International Airport',
        'newman': 'Newman Airport',
        'geraldton': 'Geraldton Airport',
        'broome': 'Broome International Airport',
        'kununurra': 'Kununurra Airport',
        'learmonth': 'Learmonth Airport',
        'carnarvon': 'Carnarvon Airport',
        'esperance': 'Esperance Airport',
        'albany': 'Albany Airport',
        'shark_bay': 'Shark Bay Airport',
        'mount_magnet': 'Mount Magnet Airport',
        'meekatharra': 'Meekatharra Airport',
        'woodie_woodie': 'Woodie Woodie Airport',
        'west_angelas': 'West Angelas Airport',
        'ginbata': 'Ginbata Airport',
        'christmas_creek': 'Christmas Creek Mine Airport',
        'granites': 'The Granites Airport'
    }

    # latitude and longitude data was taken from https://openflights.org/html/apsearch (accessed 5/10/19)
    # broome location data was slightly inaccurate so data was taken from a point on google maps
    airport_latitude = {
        'perth': -31.94029998779297,
        'sydney': -33.94609832763672,
        'melbourne': -37.673302,
        'brisbane': -27.384199142456055,
        'adelaide': -34.945,
        'cairns': -16.885799408,
        'karratha': -20.712200164799995,
        'kalgoorlie': -30.789400100699996,
        'port_hedland': -20.3777999878,
        'newman': -23.417800903299998,
        'geraldton': -28.796101,
        'broome': -17.9506085,
        'kununurra': -15.7781000137,
        'learmonth': -22.235599517799997,
        'carnarvon': -24.880211,
        'esperance': -33.684399,
        'albany': -34.94329833984375,
        'shark_bay': -25.8938999176,
        'mount_magnet': -28.116100311279297,
        'meekatharra': -26.6117000579834,
        'woodie_woodie': -21.645,
        'west_angelas': -23.136026,
        'ginbata': -22.5812,
        'christmas_creek': -22.356732,
        'granites': -20.5483
    }

    airport_longitude = {
        'perth': 115.96700286865234,
        'sydney': 151.177001953125,
        'melbourne': 144.843002,
        'brisbane': 153.11700439453125,
        'adelaide': 138.531006,
        'cairns': 145.755004883,
        'karratha': 116.773002625,
        'kalgoorlie': 121.461997986,
        'port_hedland': 118.625999451,
        'newman': 119.803001404,
        'geraldton': 114.707001,
        'broome': 122.2314674,
        'kununurra': 128.707992554,
        'learmonth': 114.088996887,
        'carnarvon': 113.67174,
        'esperance': 121.822998,
        'albany': 117.80899810791016,
        'shark_bay': 113.577003479,
        'mount_magnet': 117.84200286865234,
        'meekatharra': 118.5479965209961,
        'woodie_woodie': 121.191667,
        'west_angelas': 118.705508,
        'ginbata': 120.03553,
        'christmas_creek': 119.652074,
        'granites': 130.347
    }

    airport_csv = os.path.join(os.path.dirname(__file__), 'airport.csv')

    def __init__(self, airport_name=None, iata=None):
        key = None
        # if iata code is specified
        if iata is not None:
            for name in self.iata_codes.keys():
                if self.iata_codes[name] == iata:
                    key = name
                    break
        # else if airport name is specified
        elif airport_name is not None:
            airport_name = airport_name.replace('_', ' ')
            for airport in self.airport_names.keys():
                if airport_name.lower() in self.airport_names[airport].lower():
                    key = airport
                    break
        # else both are None and an error must be thrown
        else:
            raise(ValueError('airport_name or iata must be specified'))
        if key is None:
            # airport not found, use airport csv file
            with open(self.airport_csv, newline='') as file:
                reader = csv.DictReader(file, delimiter=',')
                for row in reader:
                    if row['iata'] == iata:
                        key = iata
                        self.airport_names[key] = row['name']
                        self.iata_codes[key] = row['iata']
                        self.airport_latitude[key] = row['latitude']
                        self.airport_longitude[key] = row['longitude']
                        break

        self.key = key
        self.name = self.airport_names[key]
        self.iata = self.iata_codes[key]
        self.latitude = self.airport_latitude[key]
        self.longitude = self.airport_longitude[key]
        self.location = {
            'latitude': self.latitude,
            'longitude': self.longitude
        }

    def __eq__(self, other):
        if isinstance(other, AirportData):
            if (self.name == other.name and
                self.iata == other.iata and
                self.longitude == other.longitude and
                    self.latitude == other.latitude):
                return True
            else:
                return False

    def __str__(self):
        return f'{self.name}\nIATA: {self.iata}\nLatitude: {self.latitude}\nLongitude: {self.longitude}'
