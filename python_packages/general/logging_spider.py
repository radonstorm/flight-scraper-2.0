# logging_spider.py - base class for all web scrapers to use
# it facilitates scraper logging so you don't have to
# Authors - Tyler Ellement, Jack Lardner
# Last Modified - 24/7/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from datetime import datetime
from urllib.parse import urlparse
import uuid
import scrapy
import requests

from scraper_middleware.logging_middleware import Logging_Middleware

from .data_models import ScraperError, ScraperLog, ScraperSession

sessionErrorsMap = {
    "fatal": "f",
    "recoverable": "r",
    "warning": "w",
    "none": "n",
}

logStatusMap = {
    "success": "S",
    "warning": "W",
    "error": "E",
    "failed": "F",
}

dataMap = {
    "airfare": "A",
    "flight status": "F",
}

errorSeverityMap = {
    "fatal": "f",
    "error": "E",
    "recoverable": "r",
    "warning": "w",
}


class LoggingSpider(scrapy.Spider):
    """
    Spider subclass with methods to keep track of logged info to be stored in the database.
    Make your spiders a subclass of this.
    IP addresses will be collected automatically during initialization, still pass **kwargs to super()__init__()

    When your scraper finishes scraping a page of results, call the `logScraping()` method to report how it went.
    If your scraper encounters an error, call the `reportError()` method to report it.
    When your scraper finishes (ie: in the `closed()` method), call the `onSessionEnd()` method.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sessionStart = datetime.utcnow()
        url = urlparse(self.link)
        self.targetSite = url.netloc
        self.logs = []
        self.errorLogs = []
        self.errors = "none"
        # find IP address of current machine
        # sends request to website that returns external IP
        self.ipAddress = requests.get('http://ifconfig.me').text

    def onSessionEnd(self):
        """
        Should be called when the scraper has finished scraping all the sites it needs to
        """
        sessionEnd = datetime.utcnow()
        scraperSession = {
            "sessionStart": self.sessionStart,
            "sessionEnd": sessionEnd,
            "targetSite": self.targetSite,
            "errors": self.errors,
            "scraperIP": self.ipAddress
        }
        self.scraperSession = ScraperSession.createDict(**scraperSession)
        self.insertIntoDatabase()

    def logScraping(self, timestamp, targetURL, flightNumber, data, dep_time, arr_time, status="success", message=None):
        """
        Should be called after each URL scraped, with the details of how the scraping went.

        timestamp should (preferably) be the value from the header of the HTTP response.
        data should be either `airfare` or `flight status`
        status should be one of the values listed on the wiki page for the database schema.
        message is optional.
        """
        log = {
            "targetSite": self.targetSite,
            "sessionStart": self.sessionStart,
            "timestamp": timestamp,
            "targetURL": targetURL,
            "flightNumber": flightNumber,
            "dep_time": dep_time,
            "arr_time": arr_time,
            "data": data,
            "message": "" if message is None else message,
            "status": status
        }
        self.logs.append(ScraperLog.createDict(**log))

    def reportError(self, timestamp, targetURL, severity, errorDetails):
        """
        Should be called whenever an error occurs, even if the scraper is able to recover.

        timestamp should (preferably) be the value from the header of the HTTP response.
        severity should be one of the values listed on the wiki page for the database schema.
        """
        err = {
            "timestamp": timestamp,
            "targetURL": targetURL,
            "severity": severity,
            "errorDetails": errorDetails,
        }
        self.errorLogs.append(ScraperError.createDict(**err))
        self.changeErrorLevel(severity)

    def changeErrorLevel(self, newError):
        """
        Used to change self.errors to a higher level if needed, based on the severity of a reported error.
        Will do nothing if the severity of the reported error is lower than a previously reported error.
        """
        if self.errors == "fatal":
            pass  # nothing can be worse than fatal
        elif self.errors == "recoverable":
            if newError == "fatal":
                self.errors = "fatal"
        elif self.errors == "warning":
            if newError == "fatal":
                self.errors = "fatal"
            elif newError == "error":
                self.errors = "recoverable"
        elif self.errors == "none":
            if newError == "fatal":
                self.errors = "fatal"
            elif newError == "error":
                self.errors = "recoverable"
            elif newError == "recoverable" or newError == "warning":
                self.errors = "warning"

    def insertIntoDatabase(self):
        log_middleware = Logging_Middleware()
        log_middleware.insert_session(
            self.scraperSession["targetSite"],
            self.scraperSession["sessionStart"],
            self.scraperSession["sessionEnd"],
            sessionErrorsMap[self.scraperSession["errors"]],
            self.scraperSession["scraperIP"]
        )
        for log in self.logs:
            log_middleware.insert_log(
                log["timestamp"],
                log["targetURL"],
                log["targetSite"],
                log["sessionStart"],
                log["flightNumber"],
                log["dep_time"],
                log["arr_time"],
                dataMap[log["data"]],
                log["message"],
                logStatusMap[log["status"]],
            )
        for err in self.errorLogs:
            log_middleware.insert_error(
                uuid.uuid4().hex,
                err["timestamp"],
                err["targetURL"],
                errorSeverityMap[err["severity"]],
                err["errorDetails"]
            )
        log_middleware.close_connection()

    def next_proxy(self, current_proxy=None):
        """ Keeps track of what proxy address have been used
            must change recorded IP address using the LPM API
            should return the next proxy address to use
        """
        proxy_addr = 'http://127.0.0.1:'
        proxies = requests.get('http://127.0.0.1:22999/api/proxies_running').json()
        # if current_port is None (haven't used a proxy yet) use the first proxy
        if current_proxy is None:
            self.ipAddress = proxies[0]['ip']
            return proxy_addr + str(proxies[0]['port'])
        else:
            current_port = int(current_proxy.split(':')[2])
            ports = []
            for proxy in proxies:
                ports.append(proxy['port'])
            current_proxy_idx = ports.index(current_port)
            # if we have used the last proxy, return None
            if current_proxy_idx == len(proxies) - 1:
                self.ipAddress = requests.get('https://ifconfig.me').text
                return None
            else:
                self.ipAddress = proxies[current_proxy_idx + 1]['ip']
                return proxy_addr + str(proxies[current_proxy_idx + 1]['port'])
