# timezones.py - Contains TimezoneConverter class scrapers can use to format scraped times
# Author - Jack Lardner
# Last Modified - 23/09/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# eg. to convert a time that is in Sydney timezone:
# time_to_utc('sydney', datetime(year=YYYY,month=M,day=D,hour=H,minute=M))

from datetime import datetime
from general.airport_data import AirportData
import pytz
import calendar
import os
import csv


# collections for locations and their timezones
timezones = {
    'perth': 'Australia/Perth',
    'sydney': 'Australia/Sydney',
    'melbourne': 'Australia/Melbourne',
    'brisbane': 'Australia/Brisbane',
    'adelaide': 'Australia/Adelaide',
    'cairns': 'Australia/Queensland',
}

# if a location is in this collection, its timezone is 'Australia/West'
wa_region = [
    'karratha',
    'kalgoorlie',
    'port_hedland',
    'newman',
    'geraldton',
    'broome',
    'kununurra',
    'learmonth',
    'carnarvon',
    'esperance',
    'albany',
    'shark_bay',
    'mount_magnet',
    'meekatharra',
]

fmt_str = '%Y-%m-%d %H:%M:%S %Z'

timezone_csv = os.path.join(os.path.dirname(__file__), 'iata_tz.csv')
csv_headers = ('iata', 'timezone')


# searches through iata_tz.csv for a timezone given an iata code
def search_csv(iata):
    with open(timezone_csv, newline='') as file:
        reader = csv.DictReader(file, fieldnames=csv_headers, delimiter='\t')
        for row in reader:
            if row['iata'] == iata:
                return row['timezone']


# given a location and a datetime, add timezone to datetime and convert to UTC
# location can be an IATA code or a city name just as AirportData takes them
# dt must be a datetime object with no timezone
# return as a formatted string with timezone info
def time_to_utc(location, dt):
    if len(location) == 3:
        airport = AirportData(iata=location)
    else:
        airport = AirportData(airport_name=location)
    # identify the timezone the location is in
    timezone = None
    if airport.name.lower() in timezones.keys():
        timezone = pytz.timezone(timezones[airport.name.lower()])
    elif airport.name.lower() in wa_region:
        timezone = pytz.timezone('Australia/West')
    else:
        timezone = pytz.timezone(search_csv(location))
    # return the datetime converted and normalized to UTC
    return timezone.normalize(timezone.localize(dt)).astimezone(pytz.utc).strftime(fmt_str)


# does the reverse of the above function
# given a datetime that is already in UTC, convert to the timezone of location
def utc_to_localtime(location, dt):
    if len(location) == 3:
        airport = AirportData(iata=location)
    else:
        airport = AirportData(airport_name=location)
    timezone = None
    if airport.name.lower() in timezones.keys():
        timezone = pytz.timezone(timezones[airport.name.lower()])
    elif airport.name.lower() in wa_region:
        timezone = pytz.timezone('Australia/West')
    else:
        timezone = pytz.timezone(search_csv(location))
    # normalize and localize datetime to UTC then convert to local timezone (reverse of above function)
    return pytz.utc.normalize(pytz.utc.localize(dt)).astimezone(timezone).strftime(fmt_str)


# Given a HTTP date header, convert to a UTC formatted string which can be inserted into the database
# HTTP date headers always follow the same format and are given in GMT
def http_header_to_utc(date_string):
    date_string.replace(',', '')
    date_array = date_string.split(' ')
    time_array = date_array[4].split(':')
    month = list(calendar.month_abbr).index(date_array[2])
    date = datetime(
        year=int(date_array[3]),
        month=month,
        day=int(date_array[1]),
        hour=int(time_array[0]),
        minute=int(time_array[1]),
        second=int(time_array[2])
    )
    return pytz.timezone('GMT').normalize(pytz.timezone('GMT').localize(date)).astimezone(pytz.utc).strftime(fmt_str)


# converts a string that was returned from the functions above into a full datetime object
# returned datetime won't have any timezone information so be careful
def str_to_dt(dt):
    return datetime.strptime(dt, fmt_str)


def time_to_24(time):
    """Accepts a string representing time in 12 hour format like so:
        '1:30 PM'. Must have suffix to determine hour calculations.
        returns a list with index [0] containing the hour and index [1] containing the minutes"""
    suffix = time.split(' ')[1].lower()
    time = time.strip(' AMP').split(':')
    time[0] = int(time[0])
    time[1] = int(time[1])
    if 'pm' in suffix and time[0] < 12:
        time[0] = time[0] + 12
    elif 'am' in suffix and time[0] == 12:
        time[0] = 0
    return time
