
from random_useragent.random_useragent import Randomize
from random import randint


# Map of user agent strings
# User agent string is set with the user_agent argument given to scrapy
os = [
    'windows',
    'mac',
    'linux'
]
user_agents = {
    'chrome': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36',
    'random': Randomize().random_agent('desktop', os[randint(0, 2)])
}
