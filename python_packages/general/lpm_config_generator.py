# lpm_config_generator
# generate a lpm config file with correct settings
# loads a template file and edits accordingly for 30 VMs
# expects 'ip_list.txt' and 'template.json' files in the same directory
import json
import copy

NUM_IPS = 90
NUM_VM = 30
MAX_PROXY = int(NUM_IPS / NUM_VM)

template = None
ip_list = None
with open('ip_list.txt', 'r') as file:
    ip_list = file.readlines()
with open('template.json', 'r') as file:
    template = file.read()

template = json.loads(template)
for vm in range(0, NUM_VM):
    config = copy.deepcopy(template)
    for i in range(0, MAX_PROXY):
        idx = (vm * MAX_PROXY) + i
        config['proxies'][0]['ips'].append(ip_list[idx].replace('\n', ''))
    with open(f'config{vm + 1}.json', 'w') as config_file:
        config_file.write(json.dumps(config))
