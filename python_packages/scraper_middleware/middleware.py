""" Middleware for Flightscraper
Author:         Ross Curley
Student id:     19098081
Last Edited:    3/10/2019
Last Edited by: Ross Curley
"""
import psycopg2
import sys
import os.path

from configparser import ConfigParser
from .exceptions import MiddlewareError

default_fn = os.path.join(os.path.dirname(__file__), "database.ini")


class Middleware:
    """ Initialise database connection """

    def __init__(self, filename=default_fn, section="postgresql"):
        try:
            self._params = self.config(filename, section)
            self._db = self.connect(self._params)
        except:
            raise

    def config(self, filename, section):
        """ Parse connection parameters from database ini file. """
        # create parser
        parser = ConfigParser()
        # read config file
        parser.read(filename)

        # get section, default to postgrsql
        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            tb = sys.exc_info()[2]
            raise MiddlewareError('Section {0} not found in the {1} file.'.format(section, filename)).with_traceback(tb)

        return db

    def connect(self, params):
        """ Connect to PostgreSQL database server. """
        conn = None
        try:
            # print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            # execute a statement
            # print('PostgreSQL database version:')
            # cur.execute('SELECT version()')

            # display the postgreSQL database server version
            # db_version = cur.fetchone()
            # print(db_version)
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise MiddlewareError("Error Connecting to Database :").with_traceback(tb)

        return conn

    def close_connection(self):
        """ Close database connection. """
        try:
            self._db.close()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise MiddlewareError(err).with_traceback(tb)
