"""Middleware for flightdata
Author:         Ross Curley, Jack Lardner
Last Edited:    30/09/20
Last Edited by: Jack Lardner
License: https://www.gnu.org/licenses/gpl-3.0.en.html
"""
import psycopg2
import sys
from .middleware import Middleware
from .exceptions import FlightdataError
from configparser import ConfigParser

"""Flightdata_Middleware Extends Middleware
used to insert error and logging information into the database
"""


class Flightdata_Middleware(Middleware):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def execute_sql(self, sql):
        """
        WARNING!!!
        This method allows execution of raw SQL to be used for testing purposes only!!!
        """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql)
            # commit the changes with the database
            self._db.commit()
            return cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Executing SQL :", err, sql).with_traceback(tb)

    def insert_airport(self, iata_code, latitude=None, longitude=None, name=None, timezone=None):
        """ Insert a new airport into the airports table. """
        sql = """
                INSERT INTO airport(iata_code, latitude, longitude, name, timezone)
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT ON CONSTRAINT airport_pkey
                DO NOTHING
                RETURNING iata_code;
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (iata_code, latitude, longitude, name, timezone))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airport :", err, sql).with_traceback(tb)

    def insert_airport_list(self, airport_list):
        """ Insert multiple airports into airports table. """
        sql = """
                INSERT INTO airport(iata_code, latitude, longitude, name, timezone)
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT ON CONSTRAINT airport_pkey
                DO NOTHING
                RETURNING iata_code;
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airport_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airport :", err, sql).with_traceback(tb)

    def insert_aircraft(self, aircraft_id='unknown', make='unknown', model='unknown', num_seats=None):
        """ Insert a new aircraft into the aircraft table. """
        sql = """
                INSERT INTO aircraft(aircraft_id, make, model, num_seats)
                VALUES(%s, %s, %s, %s)
                ON CONFLICT(aircraft_id, make, model)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (aircraft_id, make, model, num_seats))

            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Aircraft :", err, sql).with_traceback(tb)

    def insert_aircraft_list(self, aircraft_list):
        """ Insert multiple aircrafts into aircraft table. """
        sql = """
                INSERT INTO aircraft(aircraft_id, make, model, num_seats)
                VALUES(%s, %s, %s, %s)
                ON CONFLICT (aircraft_id, make, model)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, aircraft_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Aircraft :", err, sql).with_traceback(tb)

    def insert_airline(self, name, website=None):
        """ Insert a new airline into the airline table. """
        sql = """
                INSERT INTO airline(name, website)
                VALUES(%s, %s)
                ON CONFLICT (name)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (name, website))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airline :", err, sql).with_traceback(tb)

    def insert_airline_list(self, airline_list):
        """ Insert a new airline list into the airline table. """
        sql = """
                INSERT INTO airline(name, website)
                VALUES(%s, %s)
                ON CONFLICT (name)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airline_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airline :", err, sql).with_traceback(tb)

    def insert_flight(self, flight_number, destination_id, origin_id, departure_time, arrival_time):
        """ Insert a new flight entity into the flightinfo database """
        sql = """
                INSERT INTO flights(flight_number, destination_id, origin_id, departure_time, arrival_time)
                VALUES(
                    %s,
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    %s, %s
                )
                ON CONFLICT (flight_number, departure_time, arrival_time)
                DO NOTHING
        """
        try:
            cur = self._db.cursor()
            cur.execute(sql, (flight_number, destination_id, origin_id, departure_time, arrival_time))
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error inserting Flight: ", err, sql).with_traceback(tb)

    def insert_flightinfo(self, flight, aircraft, airline):
        """ Insert a new flightinfo into the flightinfo database.
            flight parameter should be a dict with the following keys:
                flight_number, departure_time, arrival_time
            aircraft parameter should be a dict with the following keys:
                aircraft_id, aircraft_make, aircraft_model"""
        sql = """
                CALL InsertFlightInfo(%s, %s, %s, %s, %s, %s, %s)
        """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # determine which information about the aircraft is given
            cur.execute(sql, (
                flight['flight_number'],
                flight['departure_time'],
                flight['arrival_time'],
                aircraft['aircraft_id'],
                aircraft['aircraft_make'],
                aircraft['aircraft_model'],
                airline
            ))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightInfo :", err, sql).with_traceback(tb)

    def insert_flightstatus(self, flight, time_collected, departure_status, arrival_status):
        """ Insert a new flightstatus into the flightstatus database. """
        sql = """
                INSERT INTO flightstatus(flight_id, time_collected, departure_status, arrival_status)
                VALUES(
                    (SELECT id FROM flights WHERE (flight_number = %s) AND (departure_time = %s) AND (arrival_time = %s)),
                    %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(
                sql, (
                    flight['flight_number'],
                    flight['departure_time'],
                    flight['arrival_time'],
                    time_collected,
                    departure_status,
                    arrival_status
                ))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightStatus :", err, sql).with_traceback(tb)

    def insert_flightstatus_list(self, flightstatus_list):
        """ Insert a new flightstatus into the flightstatus database. """
        sql = """
                INSERT INTO flightstatus(flightinfo_id, time_collected, departure_status, arrival_status)
                VALUES(
                    (SELECT id FROM flightinfo WHERE (flight_number = %s) AND (departure_time = %s) AND (arrival_time = %s)),
                    %s, %s, %s
                )
                ON CONFLICT (flightinfo_id, time_collected)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, flightstatus_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightStatus :", err, sql).with_traceback(tb)

    def insert_airfare(self, flight_number, dep_time, arr_time, time_collected, fare_class, fare, fare_type):
        """ Insert a new airfare into the airfare table. """
        sql = """
                INSERT INTO airfare(flight_id, time_collected, fare_class, fare, fare_type)
                VALUES(
                    (SELECT id FROM flights WHERE (flight_number = %s) AND (departure_time = %s) AND (arrival_time = %s)),
                    %s, %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected, fare_class, fare_type)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (flight_number, dep_time, arr_time, time_collected, fare_class, fare, fare_type))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airfare :", err, sql).with_traceback(tb)

    def insert_airfare_list(self, airfare_list):
        """ Insert a new airfare into the airfare table. """
        sql = """
                INSERT INTO airfare(flightinfo_id, time_collected, fare_class, fare, fare_type)
                VALUES(
                    (SELECT id FROM flightinfo WHERE (flight_number = %s) AND (departure_time = %s) AND (arrival_time = %s)),
                    %s, %s, %s, %s
                )
                ON CONFLICT (flightinfo_id, time_collected, fare_class, fare_type)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airfare_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airfare :", err, sql).with_traceback(tb)

    def insert_trackedroute(self, origin_id, destination_id, start_tracking, stop_tracking):
        """ Insert a new trackedroute into the trackedroutes table. """
        sql = """
                INSERT INTO trackedroutes(origin_id, destination_id, start_tracking, stop_tracking)
                VALUES(
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    %s, %s
                )
                ON CONFLICT (origin_id, destination_id, start_tracking)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (origin_id, destination_id, start_tracking, stop_tracking))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting TrackedRoutes :", err, sql).with_traceback(tb)

    def insert_trackedroute_list(self, trackedroute_list):
        """ Insert a new trackedroute into the trackedroutes table. """
        sql = """
                INSERT INTO trackedroutes(origin_id, destination_id, start_tracking, stop_tracking)
                VALUES(
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    %s, %s
                )
                ON CONFLICT (origin_id, destination_id, start_tracking)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, trackedroute_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting TrackedRoutes", err, sql).with_traceback(tb)

    def get_setting(self, key, time):
        """ Get a setting from the database, given a key and current time """
        sql = """
            SELECT value FROM scrapersettings WHERE key = %(key)s AND
            %(time)s BETWEEN valid_from AND valid_to;
        """
        retval = None
        try:
            cur = self._db.cursor()
            cur.execute(sql, {'key': key, 'time': time})
            retval = cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error getting setting: ", err, sql).with_traceback(tb)
        if retval is None:
            raise FlightdataError('Error getting setting: Setting does not exist', (), '')
        return retval[0]
