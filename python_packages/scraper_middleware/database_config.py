

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',  # change
        'USER': 'postgres',  # change
        'PASSWORD': 'flightscraper',  # change
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
