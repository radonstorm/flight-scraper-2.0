# qantas_manager.py - Scraper manager that handles calling the Qantas scraper
# Author - Jack Lardner
# Last modified - 21/10/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from datetime import datetime, timedelta
from .qantas.spiders.qantas import QantasSpider
from crochet import run_in_reactor


class QantasManager(Manager):
    routes = {
        'metro': [
            'perth-sydney',
            'perth-melbourne',
            'perth-brisbane',
            'perth-adelaide',
            'perth-cairns',
            'brisbane-sydney',
            'brisbane-melbourne'
        ],
        'regional': [
            'perth-karratha',
            'kalgoorlie-perth',
            'port_hedland-perth',
            'newman-perth',
            'geraldton-perth',
            'broome-perth',
            'kununurra-perth',
            'learmonth-perth',
            'broome-kununurra'
        ]
    }

    def __init__(self):
        super().__init__('qantas', get_project_settings(), 'qantas_start')
        self.proxy_expiry = None
        self.proxy_options = None
        self.wait_time = 1

    @run_in_reactor
    def run(self, dep, dest, date):
        print(f'scraping {date}, {dep}-{dest}')
        runner = CrawlerRunner(self.scraper_settings)
        if self.proxy_expiry is not None and self.proxy_expiry < datetime.now():
            self.proxy_options = None
            self.proxy_expiry = None
        eventual = runner.crawl(QantasSpider, dep=dep, dest=dest, travel_date=date, webdriver_options=self.proxy_options, manager=self)
        return eventual

    def set_proxy(self, proxy_options=None, proxy_capabilities=None):
        """ Set the proxy for the current session
            enables persistent proxy connection
            By default resets proxy connection to None
        """
        self.proxy_options = {
            'options': proxy_options,
            'capabilities': proxy_capabilities
        }
        if proxy_options is None and proxy_capabilities is None:
            self.proxy_options = None
        if self.proxy_expiry is not None:
            self.proxy_expiry = datetime.now() + timedelta(seconds=500)
