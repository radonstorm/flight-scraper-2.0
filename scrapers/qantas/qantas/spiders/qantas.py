# qantas.py - Spider for qantas flight data
# Author - Jack Lardner
# Last modified - 21/10/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl qantas -a dep=DEP -a dest=DEST -a travel_date=YYYY,MM,DD [-a webdriver_path=/full/path/to/webdriver]
# this spider requires that a chrome webdriver binary is located in the same directory as this file
# the chrome webdriver can be downloaded for your version of chrome here: https://sites.google.com/a/chromium.org/chromedriver/downloads
# to run in a headless environment xvfb must be used to run the entire scraper using xvfb-run
# the --headless argument given to chromedriver will cause errors on the Qantas website

import scrapy
import platform
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from scrapy.selector import Selector
import base64
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import time_to_utc, http_header_to_utc
import os.path
from pathlib import Path
from datetime import datetime
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from scrapy.exceptions import CloseSpider
import time
import calendar
from urllib.parse import urlencode


class QantasSpider(LoggingSpider):
    name = 'qantas'

    # map of fare classes to css classes
    qantas_fare_classes = {
        'Red e-Deal': 'fare_AUAURED1JQ',
        'Flex': 'fare_AUAUFL2JQ',
        'Business': 'fare_AUAUBUS',
    }
    # List of airports
    airports = [
        'perth',
        'sydney',
        'melbourne',
        'brisbane',
        'adelaide',
        'cairns',
        'karratha',
        'kalgoorlie',
        'port_hedland',
        'newman',
        'geraldton',
        'broome',
        'kununurra',
        'learmonth'
    ]
    # link to Qantas homepage
    link = 'https://www.qantas.com/au/en/book-a-trip/flights.html'

    # base url to GET query endpoint
    base_url = 'https://book.qantas.com/qf-booking/dyn/air/tripflow.redirect?'

    # init function to assign passed arguments
    def __init__(self, dest, dep, travel_date, long_loading=False, webdriver_path=None, webdriver_options=None, manager=None, debug=False, *args, **kwargs):
        super(QantasSpider, self).__init__(*args, **kwargs)
        if dep not in self.airports or dest not in self.airports:
            raise ValueError('Airport not supported')
        self.dep = AirportData(airport_name=dep)
        self.dest = AirportData(airport_name=dest)
        self.travel_day = int(travel_date.split(',')[2])
        self.travel_month = int(travel_date.split(',')[1])
        self.months = list(calendar.month_name)
        if long_loading is not False:
            long_loading = True
        self.long_loading_arg = long_loading
        if debug is not False:
            debug = True
        self.debug = debug
        self.manager = manager
        # set chromedriver path
        default_webdriver_name = 'chromedriver'
        if platform.system() == 'Windows':
            default_webdriver_name += '.exe'
        self.webdriver_path = Path(__file__).resolve().parents[3] / default_webdriver_name
        if webdriver_path is not None:
            self.webdriver_path = webdriver_path
        # set chromedriver options
        if webdriver_options is None:
            self.webdriver_options = webdriver.ChromeOptions()
            self.webdriver_options.add_argument('window_size=1920,1080')
            self.webdriver_options.add_argument('--disable-blink-features=AutomationControlled')
            self.webdriver_options.add_experimental_option('prefs', {
                'profile.managed_default_content_settings.images': 2,
                'disk-cache-size': 4096
            })
            self.webdriver_capabilities = dict()
            if not debug:
                self.webdriver_options.add_argument('--disable-gpu')
                self.webdriver_options.add_argument('--lang=en_US')
        else:
            self.webdriver_options = webdriver_options['options']
            self.webdriver_capabilities = webdriver_options['capabilities']
            # need to set correct proxy IP address
            proxyinfo = requests.get('http://127.0.0.1:22999/api/proxies_running').json()
            for proxy in proxyinfo:
                if int(self.webdriver_capabilities['proxy']['httpProxy'].split(':')[2]) == proxy['port']:
                    print(f'resuming with proxy ip {proxy["ip"]}')
                    self.ipAddress = proxy['ip']
        # set arguments given to Qantas GET query endpoint
        self.args = {
            'depAirports': self.dep.iata,
            'destAirports': self.dest.iata,
            'travelDates': travel_date.replace(',', '') + '0000',
            'numberOfAdults': '1',
            'numberOfYoungAdults': '0',
            'numberOfChildren': '0',
            'numberOfInfants': '0'
        }

    # Load homepage, input data and submit form
    def start_requests(self):
        # Load the Qantas homepage, enter information manually and submit
        yield scrapy.Request(
            self.link,
            callback=self.start_navigation
        )

    def long_loading(self, browser):
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '.css-2frk4g'))
        )
        browser.execute_script('window.scrollTo(0, 500)')
        # input departure location
        browser.find_element_by_class_name('css-1bqb9bv-departure-port__container').click()
        browser.find_element_by_class_name('css-1mu1mk2').send_keys(self.dep.iata)
        WebDriverWait(browser, 2).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '.css-1cwu89w'))
        )
        browser.find_element_by_class_name('css-1mu1mk2').send_keys(Keys.ENTER)

        # input destination location
        browser.find_element_by_class_name('css-65mysx-arrival-port__container').click()
        browser.find_element_by_class_name('css-1mu1mk2').send_keys(self.dest.iata)
        WebDriverWait(browser, 2).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '.css-1cwu89w'))
        )
        browser.find_element_by_class_name('css-1mu1mk2').send_keys(Keys.ENTER)

        # select one way
        browser.find_element_by_css_selector('.css-8oww3-DropdownMenu-DropdownMenu-overrideClassName-ButtonBase-ButtonBase-css').click()
        dropdown_one = browser.find_element_by_id('downshift-0-item-0')
        dropdown_two = browser.find_element_by_id('downshift-0-item-1')
        if dropdown_one.text == 'One way':
            dropdown_one.click()
        else:
            dropdown_two.click()

        # input travel date
        browser.find_element_by_class_name('css-1963ued-travel-dates__container').click()
        WebDriverWait(browser, 2).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '.css-11fhlh1'))
        )
        browser.execute_script('document.querySelectorAll(".css-zrbnj2-runway-calendar__day")[7].click()')
        time.sleep(1)
        browser.execute_script('document.querySelector(".css-vbrrm8-baseStyles-baseStyles-baseStyles-solidStyles-solidStyles-solidStyles-Button").click()')
        time.sleep(1)
        browser.execute_script('document.querySelector(".css-hbhwmh-baseStyles-baseStyles-baseStyles-solidStyles-solidStyles-solidStyles-Button").click()')
        time.sleep(1)

    def check_if_blocked(self, browser, link, old_link):
        """Check if Qantas is blocking scraper
        If it is blocked activate a proxy, rotate to a different proxy or disable the proxy
        """
        blocked = True
        currentBrowser = browser
        while blocked:
            try:
                currentBrowser.get(link)
                WebDriverWait(currentBrowser, 7).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'gerr-message'))
                )
                # get current proxy
                proxy = None
                if 'proxy' in self.webdriver_capabilities:
                    proxy = self.webdriver_capabilities['proxy']['httpProxy']
                # ask for the next proxy given current port
                next_proxy_addr = self.next_proxy(proxy)
                print(f'setting to proxy address available on port {next_proxy_addr}')
                new_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
                # if we rotate to another proxy add to new_capabilities if not dont add anything
                if next_proxy_addr is not None:
                    new_proxy = Proxy({
                        'proxyType': ProxyType.MANUAL,
                        'httpProxy': next_proxy_addr,
                        'ftpProxy': next_proxy_addr,
                        'sslProxy': next_proxy_addr,
                        'noProxy': ''
                    })
                    new_proxy.add_to_capabilities(new_capabilities)
                    # set proxy information for future scrapes
                    self.manager.set_proxy(proxy_options=self.webdriver_options, proxy_capabilities=new_capabilities)
                elif next_proxy_addr is None:
                    # if we are disabling proxy, also disable it for future scrapes
                    self.manager.set_proxy()
                    print(f'disabling proxy')
                # create new browser with new capabilities
                self.webdriver_capabilities = new_capabilities
                currentBrowser.delete_all_cookies()
                currentBrowser.quit()
                currentBrowser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options, desired_capabilities=new_capabilities)
                currentBrowser.set_window_size(1920, 1080)
                time.sleep(1)
                currentBrowser.get(old_link)
            except TimeoutException:
                print('Blocked error message not found, continuing...')
                blocked = False
        return currentBrowser

    def start_navigation(self, response):
        browser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options, desired_capabilities=self.webdriver_capabilities)
        browser.set_window_size(1920, 1080)
        browser.get(self.link)
        old_link = self.link
        if self.long_loading_arg:
            self.long_loading(browser)

        self.link = self.base_url + urlencode(self.args)
        browser = self.check_if_blocked(browser, self.link, old_link)
        # use a browser wait instead of sleeping
        try:
            # wait a max of 10 seconds for flight table rows to be available
            WebDriverWait(browser, 30).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, '.row.itinerary'))
            )
        except TimeoutException:
            # check here if there is no flight data and log accordingly
            try:
                no_flights = browser.find_element(By.CSS_SELECTOR, '.card-block.error-message-centered-text')
                print('No flights available on this day')
            except NoSuchElementException:
                print('Flight info not loaded in time')
                timestamp = datetime.utcnow()
                self.reportError(timestamp, self.link, 'fatal', 'Could not load search results')
                self.logScraping(
                    timestamp,
                    self.link,
                    'ERROR',
                    'airfare',
                    '2030-01-01 00:00:00',
                    '2030-01-01 01:00:00',
                    'failed',
                    'Flight info not loaded in time'
                )
        if self.debug:
            browser.save_screenshot(os.path.join(os.path.dirname(__file__), f'{self.dep.iata}-{self.dest.iata}-{self.travel_day}{self.travel_month}-flights.png'))
        # create selector from document html
        html = Selector(text=browser.page_source)
        # isolate every flight row
        flight_data = html.xpath('//*[@class="row itinerary"]')
        flights = []
        for index, flight in enumerate(flight_data):
            # check if flight has a layover and is a Qantas flight, if not continue
            if(
                len(flight.xpath('.//upsell-segment-details').getall()) == 1 and  # ensures only one flight, no layovers
                flight.xpath('.//*[@class="e2e-flight-number"]/text()').get().startswith('QF')  # ensures an actual Qantas flight, not Jetstar or affiliate
            ):

                # scrape all information
                flight_number = flight.xpath(
                    './/*[@class="e2e-flight-number"]/text()'
                ).get()
                fares = {}
                for fare_class in self.qantas_fare_classes:
                    price = flight.xpath(
                        f'.//*[contains(@class, "{self.qantas_fare_classes[fare_class]}")]//*[@class="amount cash ng-star-inserted"]/text()'
                    ).get()
                    if price is not None:
                        price = int(price.replace('$', '').replace(',', ''))
                    sold_out = flight.xpath(
                        f'.//*[contains(@class, "{self.qantas_fare_classes[fare_class]}")]//*[@class="amount no-seat"]/text()'
                    ).get()
                    if sold_out is not None:
                        price = sold_out
                    elif price is None:
                        price = 'N/A'
                    fares[fare_class] = price
                # scrape flight details
                browser.execute_script(f'document.querySelectorAll(".e2e-flight-number")[{index}].click()')
                try:
                    check_detail_available = WebDriverWait(browser, 13).until(
                        EC.presence_of_element_located((By.CSS_SELECTOR, '.list-inline.flight-info-details-panel.col-12.col-lg-8'))
                    )
                    # scrape detail information here
                    detail_html = Selector(text=browser.page_source)
                    flight_html = detail_html.xpath(
                        '//*[@class="list-inline flight-info-details-panel col-12 col-lg-8"]'
                    )
                    departure_date = flight_html.xpath('.//tr[2]/td[2]/text()').get().split(' ')
                    departure_time = flight_html.xpath('.//tr[3]/td[2]/text()').get().split(':')
                    arrival_date = flight_html.xpath('.//tr[5]/td[2]/text()').get().split(' ')
                    arrival_time = flight_html.xpath('.//tr[6]/td[2]/text()').get().split(':')

                    departure_datetime = datetime(
                        int(departure_date[3]), int(self.months.index(departure_date[2])), int(departure_date[1]),
                        int(departure_time[0]), int(departure_time[1])
                    )
                    arrival_datetime = datetime(
                        int(arrival_date[3]), int(self.months.index(arrival_date[2])), int(arrival_date[1]),
                        int(arrival_time[0]), int(arrival_time[1])
                    )

                    aircraft = flight_html.xpath('.//tr[10]/td[2]/text()').get()

                    flight_info = {
                        'flight_number': flight_number,
                        'departure_time': time_to_utc(self.dep.iata, departure_datetime),
                        'arrival_time': time_to_utc(self.dest.iata, arrival_datetime),
                        'origin': self.dep.iata,
                        'destination': self.dest.iata,
                        'aircraft_id': 'unknown',
                        'aircraft_make': aircraft.split(' ')[0],
                        'aircraft_model': aircraft.split(' ')[1],
                        'fares': fares
                    }
                    yield(flight_info)
                    timestamp = datetime.utcnow()
                    self.logScraping(
                        timestamp,
                        self.link,
                        flight_info['flight_number'],
                        'airfare',
                        flight_info['departure_time'],
                        flight_info['arrival_time'],
                        'success'
                    )
                    flights.append(flight_info)
                except TimeoutException:
                    print(f'Took too long to render flight detail for flight {flight_number}')
                    if self.debug:
                        browser.save_screenshot(os.path.join(os.path.dirname(__file__), f'detail-render-{flight_number}-{self.travel_day}{self.travel_month}.png'))
                    timestamp = datetime.utcnow()
                    self.reportError(timestamp, self.link, 'error', f'Could not render flight details for flight {flight_number}')
                    self.logScraping(
                        timestamp,
                        self.link,
                        'ERROR',
                        'airfare',
                        '2030-01-01 00:00:00',
                        '2030-01-01 01:00:00',
                        'error',
                        f'Took too long to render flight detail for flight {flight_number}'
                    )
                browser.execute_script('document.querySelector(".modal-close").click()')
                time.sleep(1)

            # else remaining flights have layovers, skip
            elif(len(flight.xpath('.//upsell-segment-details').getall()) > 1):
                break
        browser.delete_all_cookies()
        browser.quit()
        # scraped everything, insert data
        middleware = Flightdata_Middleware()
        time_now = datetime.utcnow()
        print(f'only have {len(flights)} flights')
        for flight in flights:
            middleware.insert_flight(
                flight['flight_number'],
                flight['destination'],
                flight['origin'],
                flight['departure_time'],
                flight['arrival_time']
            )

            for fare_class in flight['fares']:
                if flight['fares'][fare_class] != 'N/A':
                    middleware.insert_airfare(
                        flight['flight_number'],
                        flight['departure_time'],
                        flight['arrival_time'],
                        time_now,
                        fare_class,
                        flight['fares'][fare_class],
                        'one way'
                    )

            middleware.insert_aircraft(
                aircraft_id=flight['aircraft_id'],
                make=flight['aircraft_make'],
                model=flight['aircraft_model'],
                num_seats=None
            )

            middleware.insert_flightinfo(
                flight,
                {
                    'aircraft_id': flight['aircraft_id'],
                    'aircraft_make': flight['aircraft_make'],
                    'aircraft_model': flight['aircraft_model']
                },
                'Qantas'
            )
        self.onSessionEnd()
