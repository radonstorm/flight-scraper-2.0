-- qantas_script.lua - Lua script for Splash to scrape flight information from Qantas
-- Author - Jack Lardner
-- Last modified - 28/08/20
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html
-- This script is unmaintained and not used anymore, Qantas spider now uses Selenium for webpage rendering

function main(splash, args)
    function insert_text(field, text)
        field:send_text(text)
        assert(splash:wait(0.5))
        field:send_keys('<Return>')
        assert(splash:wait(0.1))
    end

    -- returns true if start is at the beginning of str
    function str_starts_with(str, start)
        return str:sub(1, #start) == start
    end

    -- JS function to select one way trips
    local select_one_way = splash:jsfunc([[
        function(){
            var dropdownOne = document.getElementById("downshift-0-item-0");
            var dropdownTwo = document.getElementById("downshift-0-item-1");
            if(dropdownOne.textContent === "One way")
            {
                return dropdownOne;
            }
            else
            {
                return dropdownTwo;
            }
        }
    ]])

    --[[
        JS function to select travel date
        if date is not found, false is returned
        month is full month name Capitalized eg 'September'
        day is day number eg '15'
    ]]
    local select_date = splash:jsfunc([[
        function(day, travelMonth) {
            day = String(day);
            var selectedDay = false;
            var months = document.querySelectorAll('.css-fls1na-Month');
            for(ii = 0; ii < months.length; ii++){
                if(travelMonth.startsWith(months[ii].textContent))
                {
                    var selectedMonth = months[ii].parentNode.parentNode;
                    days = selectedMonth.querySelectorAll('.css-1hgseo0-runway-calendar__date');
                    for(ii = 0; ii < days.length; ii++){
                        if(days[ii].innerHTML === day){
                            // days[ii].click();
                            // document.querySelector('.css-u8f74z-DialogHeader').click();
                            selectedDay = days[ii];
                        }
                    }
                }
            }
            return selectedDay;
        }
    ]])

    splash:set_user_agent(args.user_agent)
    splash:init_cookies(args.cookies)
    -- splash:set_viewport_size(1920, 1080)
    splash:set_viewport_full()
    -- splash.images_enabled = false
    assert(splash:go(args.link))
    assert(splash:wait(3))

    -- add data to form
    -- set one-way
    local one_way_dropdown = splash:evaljs('document.querySelector(".css-1kqica7-runway-dropdown__container")')
    one_way_dropdown:mouse_click()
    assert(splash:wait(0.3))
    select_one_way():mouse_click()
    assert(splash:wait(0.3))

    -- check elements exist, if not the layout could've changed
    local dep_dropdown = splash:select('.css-1bqb9bv-departure-port__container')
    local dest_dropdown = splash:select('.css-65mysx-arrival-port__container')
    if dep_dropdown == nil or dest_dropdown == nil then
        return {
            url = splash:url(),
            search_form_unavailable = true
        }
    end

    -- set departure and destination
    dep_dropdown:mouse_click()
    assert(splash:wait(0.3))
    insert_text(splash:select('.css-1mu1mk2'), args.dep)
    dest_dropdown:mouse_click()
    assert(splash:wait(0.3))
    insert_text(splash:select('.css-1mu1mk2'), args.dest)
    splash:set_viewport_full()
    -- set departure date
    splash:select('.css-5xbxpx-runway-popup-field__button'):mouse_click()
    assert(splash:wait(5))
    local selectedDay = select_date(args.day, args.month)
    assert(splash:wait(5))
    if selectedDay then
        splash:set_viewport_full()
        selectedDay:mouse_click()
    end
    assert(splash:wait(1))
        local closeButton = splash:select('.css-u8f74z-DialogHeader')
        closeButton:mouse_click()
        assert(splash:wait(1))
    end
    return {
        search_result_png = splash:png(),
        search_form_unavailable = false
    }

    -- set business class if melbourne flag is set
    if args.melbourne then
        local class_dropdown = splash:select('#select-input-travelClass')
        class_dropdown:mouse_click()
        assert(splash:wait(1))
        local business_class = splash:select('#select-picker-select-input-travelClass2')
        business_class:mouse_click()
        assert(splash:wait(1))
    end

    -- submit form
    local submit_button = splash:evaljs('document.querySelectorAll(".qfa1-submit-button__button")[1]')
    submit_button:mouse_click()
    splash:wait(25)

    -- set flight filter
    splash:select('#flight-filters-button-bound0'):mouse_click()
    splash:wait(5)
    splash:select('.btn.e2e-0.lighter-display.ng-star-inserted'):mouse_click()
    splash:wait(0.5)
    splash:select('.btn.btn-secondary.apply-button'):mouse_click()
    splash:wait(1)

    -- snapshot result page
    local search_result_html = splash:html()
    local search_result_png = splash:png()

    local detail_html = {}
    local detail_png = {}
    -- go through details of flights
    local detail_links = splash:select_all('.e2e-flight-number')
    for i, link in ipairs(detail_links) do
        flight_num = link:text()
        if str_starts_with(flight_num, 'QF') then
            link:mouse_click()
            splash:wait(30)
            detail_html[flight_num] = splash:html()
            detail_png[flight_num] = splash:png()
            splash:select('#full-flight-details-modal_cls'):mouse_click()
            splash:wait(2)
        end
    end

    return {
        search_result_html = search_result_html,
        search_result_png = search_result_png,
        detail_html = detail_html,
        detail_png = detail_png,
        cookies = splash:get_cookies()
    }
end