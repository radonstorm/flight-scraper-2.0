# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import general.scrapy_items as items
from scraper_middleware.flightdata_middleware import Flightdata_Middleware

middleware = Flightdata_Middleware()


class AirnorthPipeline(object):
    def process_item(self, item, spider):
        if isinstance(item, items.Airfare):
            middleware.insert_airfare(**dict(item))
        elif isinstance(item, items.FlightInfo):
            middleware.insert_flightinfo(**dict(item), make="unknown", model="unknown")
        elif isinstance(item, items.FlightStatus):
            middleware.insert_flightstatus(**dict(item))
