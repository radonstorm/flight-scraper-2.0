-- form_script.lua - Script for splash browser to fill out Airnorth form
-- Authors - Tyler Ellement, Jack Lardner
-- Last modified - 10/11/20

function main(splash)
    splash:init_cookies(splash.args.cookies)
    assert(splash:go{
        splash.args.url,
        headers=splash.args.headers,
        http_method=splash.args.http_method,
        body=splash.args.body,
        })
    assert(splash:wait(3.0))

    -- check if blocked
    local form = splash:select("#aspnetForm")
    if form == nil then
        local body = splash:select("body")
        local content = body:text()
        if string.find(content, "blocked") == nil then
            return {
                url = splash:url(),
                no_form=true,
                cause="unknown",
            }
        else
            return {
                url = splash:url(),
                no_form=true,
                cause="blocked",
            }
        end
    else
        local originField = splash:select('#ctl00_ContentPlaceHolder1_ddlOrigin')
        local destField = splash:select('#ctl00_ContentPlaceHolder1_ddlDestination')
        local dateMonth = splash:select('#ctl00_ContentPlaceHolder1_ddlDepMonth')
        local dateDay = splash:select('#ctl00_ContentPlaceHolder1_ddlDepDay')
        local one_way = splash:select('#ctl00_ContentPlaceHolder1_rbOneWay')
        
        -- start sending text to form elements manually
        originField:send_text(splash.args.origin)
        destField:send_text(splash.args.destination)
        dateMonth:send_text(splash.args.month)
        dateDay:send_text(splash.args.day)
        one_way:mouse_click()
        form_vals = assert(form:form_values())
        form_vals['ctl00$ContentPlaceHolder1$ddlDepDay'] = splash.args.day
        assert(form:fill(form_vals))
        before = splash:png()

        -- submit form
        local sub = splash:select("#ctl00_ContentPlaceHolder1_btnContinue")
        assert(sub:mouse_click())
        splash:wait(5)
        local entries = splash:history()
        local last_response = entries[#entries].response
        return {
            url = splash:url(),
            headers = last_response.headers,
            http_status = last_response.status,
            html = splash:html(),
            png = splash:png(),
            before_png = before,
            dest_unavailable = false,
            no_form=false
        }
    end
end
