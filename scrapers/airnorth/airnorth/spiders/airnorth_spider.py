"""
To run this spider:
scrapy crawl airnorth -a origin=<origin> -a dest=<destination> [-a year=<year> -a month=<month> -a day=<day> -a numDays=<number of days> ]

<origin> and <destination> must be IATA codes.
year/month/day need to be either all specified or none specified, they specify what day to start scraping
    if omitted, it will start from today
numDays specifies how many days from the starting date the scraper will run
    if omitted, it will run for 60 days

Note: the spider expects there to be a flight in the database with a flight number of "unknown"
    It uses this to report errors in cases where it doesn't have a specific flight to report errors for.
Also, because the site doesn't have info about the aircraft, there needs to be an aircraft in the database with a make and model of "unknown"
"""


import datetime
import os.path
import time
import base64

import scrapy
from scrapy.spidermiddlewares.httperror import HttpError
from scrapy_splash import SplashFormRequest, SplashRequest

from general.airport_data import AirportData
from general.logging_spider import LoggingSpider
from general.user_agents import user_agents
from general.timezones import time_to_utc, http_header_to_utc
from scraper_middleware.flightdata_middleware import Flightdata_Middleware

fn = os.path.join(os.path.dirname(__file__), "form_script.lua")
with open(fn) as file:
    script = file.read()


def classXpath(classText):
    return r'./td[contains(@class, "' + classText + r'")]'


splash_args = {
    'wait': 12,
    'html': 1,
    'png': 1,
    'viewport': 'full'
}


class InvalidArgError(Exception):
    pass


class AirnorthSpider(LoggingSpider):
    name = "airnorth"
    link = "https://secure.airnorth.com.au/AirnorthIBE/OnlineBooking.aspx"
    auth = 'Basic ' + base64.encodebytes(b'user:userpass').decode('utf-8').strip()

    def __init__(self, user_agent="chrome", origin=None, dest=None, year=None, month=None, day=None, debug=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_agent = user_agents[user_agent]
        self.argErrors = False
        self.argErrorList = []
        if debug is not False:
            debug = True
        self.debug = debug
        try:
            if origin is None or dest is None:
                if origin is None:
                    self.argErrorList.append("origin")
                if dest is None:
                    self.argErrorList.append("dest")
                raise InvalidArgError()
            else:
                self.origin = origin
                self.originInfo = AirportData(iata=origin)
                self.dest = dest
                self.destInfo = AirportData(iata=dest)
            if year is not None and month is not None and day is not None:
                self.date = datetime.date(int(year), int(month), int(day))
            elif year is None and day is None and month is None:
                self.date = datetime.date.today()
            else:
                if year is None:
                    self.argErrorList.append("year")
                if month is None:
                    self.argErrorList.append("month")
                if day is None:
                    self.argErrorList.append("day")
                raise InvalidArgError()
        except InvalidArgError:
            self.argErrors = True

    def start_requests(self):
        if not self.argErrors:
            dateToScrape = self.date
            form_vals = {
                "origin": self.originInfo.name.split(' ')[0],
                "destination": self.destInfo.name.split(' ')[0],
                "month": f"{dateToScrape:%b %Y}",
                "day": dateToScrape.strftime('%a') + ' ' + str(int(dateToScrape.strftime('%d'))),
            }
            yield SplashRequest(
                self.link,
                endpoint="execute",
                args=dict(form_vals, lua_source=script),
                callback=self.parse_results,
                cb_kwargs={
                    "origin": self.origin,
                    "destination": self.dest,
                    "flightDate": dateToScrape,
                },
                meta={
                    "AN_retry_counter": 0
                },
                splash_headers={
                    'Authorization': self.auth
                }
            )

    def parse_results(self, response, origin, destination, flightDate):
        if self.debug:
            imgdata = base64.b64decode(response.data['png'])
            filename = 'airnorth-' + str(self.date) + '-' + self.originInfo.iata + '-' + self.destInfo.iata + '.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)
            with open('before' + filename, 'wb') as f:
                f.write(base64.b64decode(response.data['before_png']))
        self.targetSite = response.url
        status = "success"
        if response.data["no_form"]:
            endUrl = response.data["url"]
            timestamp = datetime.datetime.utcnow()
            if response.data["cause"] == "blocked":
                self.logScraping(timestamp, endUrl, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Scraper has been blocked")
                self.reportError(timestamp, endUrl, "fatal", f"It appears that the scraper has been blocked. No further requests will be sent this run")
                raise scrapy.exceptions.CloseSpider("Blocked")
            else:
                self.logScraping(timestamp, endUrl, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Unable to find form")
                self.reportError(timestamp, endUrl, "fatal", f"Could not find the form for unknown reason. Maybe the site format has changed? No further requests will be sent this run")
                raise scrapy.exceptions.CloseSpider("No Form")
        elif response.data["dest_unavailable"]:
            # This error is fine because it probably just means that airnorth doesn't offer flights on this route at all
            # The scraper should just silently ignore the route and proceed onto the next one without raising an error
            print("Destination unavailable for given origin")
            raise scrapy.exceptions.CloseSpider("route unavailable")
        else:
            timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
            validationErrs = response.xpath(r'//div[@id="ctl00_ContentPlaceHolder1_ValidationSummary1"]/ul/li/text()').getall()
            if(len(validationErrs) > 0):
                print("Validation errors in the form")
                print(validationErrs)
                self.logScraping(timestamp, self.link, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "error", f"Validation errors in the form")
                self.reportError(timestamp, self.link, "fatal", f"Validation errors in the form. Maybe the site format has changed?:\n {validationErrs}")
                raise scrapy.exceptions.CloseSpider("Validation Errors")
            else:
                tbls = response.xpath(r'//*[@id="ctl00_ContentPlaceHolder1_tblOutboundFlights"]/tbody')
                if len(tbls) > 0:  # check if the table body exists

                    tbl = tbls[0]

                    # getting the names of the fare classes
                    fare_class_headings = tbl.xpath(r'./tr[1]/th[starts-with(@class, "fare-family-header-")]')
                    fare_classes = {}
                    for head in fare_class_headings:
                        classNum = head.xpath(r'./@class').get().split('-')[-1]
                        fareName = head.xpath(r'./text()').get()
                        fare_classes[classNum] = fareName

                    # extracting the airfare data from the table
                    data = tbl.xpath(r'./tr[position()>1]')
                    middleware = Flightdata_Middleware()
                    for row in data:
                        # table structure has changed, had to change the method of checking if flight had a stop or not
                        nStops = row.xpath(classXpath("Stops") + r'/text()').get()
                        ports = row.xpath(classXpath("PortName") + r'/text()').getall()
                        if nStops == '0' and ports[0] in self.originInfo.name and ports[2] in self.destInfo.name:
                            flightNum = row.xpath(classXpath("FlightNumber") + r'/text()').get()
                            dep_datetime = self.parseTime(self.date, ports[1], self.originInfo.iata)
                            arr_datetime = self.parseTime(self.date, ports[3], self.destInfo.iata)
                            middleware.insert_flight(
                                flightNum,
                                self.destInfo.iata,
                                self.originInfo.iata,
                                dep_datetime,
                                arr_datetime
                            )
                            middleware.insert_flightinfo(
                                {
                                    'flight_number': flightNum,
                                    'departure_time': dep_datetime,
                                    'arrival_time': arr_datetime
                                },
                                {
                                    'aircraft_id': 'unknown',
                                    'aircraft_make': 'unknown',
                                    'aircraft_model': 'unknown'
                                },
                                'AirNorth'
                            )
                            airfare = {}
                            for clsNum in fare_classes:
                                fare = row.xpath(classXpath("fare-family-" + clsNum) + r'/label/text()').get()
                                if fare is not None:
                                    airfare[fare_classes[clsNum]] = fare.strip(' $').replace(',', '')
                                    middleware.insert_airfare(
                                        flightNum,
                                        dep_datetime,
                                        arr_datetime,
                                        timestamp,
                                        fare_classes[clsNum],
                                        fare.strip(' $').replace(',', ''),
                                        'one way'
                                    )
                                else:
                                    print(f'No airfare for {fare_classes[clsNum]} available')
                                    airfare[fare_classes[clsNum]] = 'Sold out'
                            flight = {
                                'flight_number': flightNum,
                                'departure_iata': self.originInfo.iata,
                                'destination_iata': self.destInfo.iata,
                                'departure_time': dep_datetime,
                                'arrival_time': arr_datetime,
                                'airfare': airfare
                            }
                            self.logScraping(
                                timestamp,
                                self.link,
                                flightNum,
                                "airfare",
                                dep_datetime,
                                arr_datetime,
                                status
                            )
                            yield(flight)
                    middleware.close_connection()
                else:
                    # if the table body doesn't exist, then there are no flights for the given day
                    print("No flights")

    def errorHandler(self, failure):
        response = failure.value.response
        self.targetSite = response.url
        if failure.check(HttpError):
            timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
            self.logScraping(timestamp, self.link, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Http error received")
            self.reportError(timestamp, self.link, "error", f"Received HTTP status: {response.status}")

    def closed(self, reason):
        if self.argErrors:
            # Scraper didn't start due to invalid arguments
            print("Finished due to invalid arguments")
            timestamp = datetime.datetime.utcnow()
            self.logScraping(timestamp, self.link, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Ended early due to invalid arguments")
            self.reportError(timestamp, self.link, "fatal", f"Ended early due to invalid arguments: {self.argErrorList}")
            self.onSessionEnd()
        elif reason in ["Validation Errors", "No Form", "Blocked"]:
            # known errors, they have already been reported when they occurred
            self.onSessionEnd()
        elif reason == "route unavailable":
            pass  # then just end without returning any items
        elif reason == "finished":
            self.onSessionEnd()
        else:
            # then error is unknown and will be logged here
            timestamp = datetime.datetime.utcnow()
            self.logScraping(timestamp, self.link, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Ended early due to unknown error")
            self.reportError(timestamp, self.link, "fatal", f"Terminated unexpectedly due to unknown error: {reason}")
            self.onSessionEnd()

    def parseTime(self, date, timestr, airport):
        parts = timestr.split(":")
        hours, mins = int(parts[0]), int(parts[1])
        timestamp = datetime.datetime(date.year, date.month, date.day, hours, mins)
        timeStr = time_to_utc(airport, timestamp)
        return timeStr
