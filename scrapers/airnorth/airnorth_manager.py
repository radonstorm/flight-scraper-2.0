# airnorth_manager.py - Scraper manager that handles calling the AirNorth scraper
# Author - Jack Lardner
# Last modified - 19/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from datetime import datetime
from .airnorth.spiders.airnorth_spider import AirnorthSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from crochet import run_in_reactor
import time


class AirnorthManager(Manager):
    routes = {
        'metro': [
            'PER-DRW',
            'DRW-MEL',
            'CNS-DRW',
            'CNS-MEL'
        ],
        'regional': [
            'DRW-BME',
            'PER-KNX',
            'PER-BME',
            'BME-KNX',
            'DRW-KNX'
        ]
    }

    def __init__(self):
        super().__init__('airnorth', get_project_settings(), 'airnorth_start')
        self.wait_time = 5

    @run_in_reactor
    def run(self, dep, dest, date):
        print(f'scraping {date} {dep}-{dest}')
        date = date.split(',')
        year = date[0]
        month = date[1]
        day = date[2]
        runner = CrawlerRunner(self.scraper_settings)
        eventual = runner.crawl(AirnorthSpider, origin=dep, dest=dest, year=year, month=month, day=day)
        return eventual
