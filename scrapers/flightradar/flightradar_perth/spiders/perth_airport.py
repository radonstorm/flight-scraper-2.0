# perth_airport.py - Spider for scraping flight status for Perth airport and some regional routes
# Author - Jack Lardner
# Last Modified - 30/09/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# This spider was requested in an attempt to start quickly collecting useful data from Perth Airport and some regional routes
# TODO: filter out unknown airlines, aircrafts and scheduled status with no time associated

import scrapy
from scrapy_splash import SplashRequest
import base64
import os.path
from datetime import date, datetime
import calendar
from pytz import UnknownTimeZoneError
from general.timezones import time_to_utc, str_to_dt, http_header_to_utc
from general.airport_data import AirportData
from general.user_agents import user_agents
from general.logging_spider import LoggingSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class PerthAirportSpider(LoggingSpider):
    name = 'perth-airport'
    start_urls = [
        'http://www.flightradar24.com/data/airports/per',
        'http://www.flightradar24.com/data/airports/bme',
        'http://www.flightradar24.com/data/airports/knx',
        'http://www.flightradar24.com/data/airports/fiz',
        'http://www.flightradar24.com/data/airports/hcq',
        'http://www.flightradar24.com/data/airports/mkr',
        'http://www.flightradar24.com/data/airports/wun',
        'http://www.flightradar24.com/data/airports/lno',
        'http://www.flightradar24.com/data/airports/lvo',
        'http://www.flightradar24.com/data/airports/cvq',
        'http://www.flightradar24.com/data/airports/mjk',
        'http://www.flightradar24.com/data/airports/mmg'
    ]

    allowed_regional_airports = [
        'BME',
        'KNX',
        'FIZ',
        'HCQ',
        'MKR',
        'WUN',
        'LNO',
        'LVO',
        'CVQ',
        'MJK',
        'MMG',
        # add PER just in case, should be picked up by duplicate filter anyway
        'PER'
    ]

    # link used for LoggingSpider logging
    link = 'http://www.flightradar24.com/data/airports/per/'

    script_file = os.path.join(os.path.dirname(__file__), 'perth-flightradar-script.lua')
    with open(script_file) as file:
        lua_script = file.read()

    splash_args = {
        'lua_source': lua_script,
        'timeout': 400,
        'wait': 10,
        'viewport': '1920x1080',
        'images': 0,
        'user_agent': user_agents['chrome']
    }

    # auth string for Authorization header
    auth = 'Basic ' + base64.encodebytes(b'user:userpass').decode('utf-8').strip()

    def __init__(self, debug=False, *args, **kwargs):
        super(PerthAirportSpider, self).__init__(*args, **kwargs)
        self.debug = debug
        self.month_name = list(calendar.month_abbr)
        self.mw = Flightdata_Middleware()

    def start_requests(self):
        append = [
            '/arrivals',
            '/departures'
        ]
        for link in self.start_urls:
            for extension in append:
                yield SplashRequest(
                    url=link + extension,
                    callback=self.parse,
                    endpoint='execute',
                    args=self.splash_args,
                    cache_args=['lua_source'],
                    splash_headers={
                        'Authorization': self.auth
                    }
                )

    # grabs all the information from the initial page (perth arrival/departures)
    # from here it goes and scrapes an additional page for missing information
    def parse(self, response):
        if self.debug:
            png = response.data['png']
            imgdata = base64.b64decode(png)
            filename = response.url.split('/')[-2:-1][0] + '-' + response.url.split('/')[-1:][0]
            with open(f'{filename}.png', 'wb') as f:
                f.write(imgdata)
        print(f'Scraping {response.url.split("/")[-2:-1][0].upper()}-{response.url.split("/")[-1:][0]} airport schedule table')
        table = response.xpath('/html/body/div[6]/div[2]/section/div/section/div/div[2]/div/aside/div[1]/table/tbody/tr')
        for flight in table:
            current_date = date.today()
            # date separator row
            if flight.attrib['class'] == 'row-date-separator hidden-xs hidden-sm ':
                flight_date = flight.xpath('./td/text()').get().split(' ')
                flight_date[1] = self.month_name.index(flight_date[1])
                current_date = date(year=current_date.today().year, month=int(flight_date[1]), day=int(flight_date[2]))
            # flightinfo row
            elif flight.attrib['class'] == 'hidden-xs hidden-sm ng-scope':
                time = flight.xpath('.//td[@class="ng-binding"]/text()').get().strip(' AMPM').split(':')
                flight_num = flight.xpath('.//td[@class="p-l-s cell-flight-number"]/a/text()').get()
                link = 'http://flightradar24.com' + flight.xpath('.//td[@class="p-l-s cell-flight-number"]/a/@href').get()
                location = flight.xpath('.//span[@class="hide-mobile-only ng-binding"]/../a/text()').get()
                airline = flight.xpath('.//td[@class="cell-airline"]/a/text()').get()
                aircraft = flight.xpath('.//span[@class="notranslate ng-binding"]/../a/text()').get()
                status = flight.xpath('.//span[@class="ng-binding"]/text()').get() + ' ' + flight.xpath('.//td[7]/text()').get()
                # string parsing
                if location is not None:
                    location = location.strip('() ')
                if airline is not None:
                    airline = airline.strip()
                if aircraft is not None:
                    aircraft = aircraft.strip('() ')
                # time is guaranteed to be Perth time so convert to UTC
                # this is also true as the regional airport will also have Perth timezone
                time = time_to_utc(
                    'PER',
                    datetime(
                        year=current_date.year,
                        month=current_date.month,
                        day=current_date.day,
                        hour=int(time[0]),
                        minute=int(time[1])
                    )
                )

                flight_status = {
                    'flight_number': flight_num,
                    'airline': airline,
                    'aircraft': aircraft,
                    'status': status,
                    'link': link,
                    'origin_link': response.url
                }
                # add info depending if it's a departure or arrival
                airport = response.url.split('/')[-2:-1][0].upper()
                if 'arrivals' in response.url:
                    flight_status['destination'] = airport
                    flight_status['origin'] = location
                    flight_status['arrival_time'] = time
                elif 'departures' in response.url:
                    flight_status['destination'] = location
                    flight_status['origin'] = airport
                    flight_status['departure_time'] = time
                self.splash_args['html'] = 1
                if self.debug:
                    self.splash_args['png'] = 1
                # scrape flight status if we're on a Perth page
                # or we're on a regional airport page and it's an allowed airport
                if ('per' in response.url.lower()) or (location in self.allowed_regional_airports):
                    yield SplashRequest(
                        flight_status['link'],
                        callback=self.parse_flightstatus,
                        endpoint='render.json',
                        args=self.splash_args,
                        splash_headers={
                            'Authorization': self.auth
                        },
                        cb_kwargs={
                            'flight_status_info': flight_status
                        }
                    )

    def parse_flightstatus(self, response, flight_status_info):
        if self.debug:
            png = response.data['png']
            imgdata = base64.b64decode(png)
            filename = flight_status_info['flight_number'] + '-status'
            with open(f'{filename}.png', 'wb') as f:
                f.write(imgdata)
        table = response.xpath('.//table[@id="tbl-datatable"]/tbody/tr')
        print('Scraping flight ' + flight_status_info['flight_number'])
        # loop through each flight in the table
        for flight in table:
            # make a clean copy of the flight_info to ensure we're
            # working with a clean set of data at the beginning of each loop
            flight_status = flight_status_info.copy()
            try:
                # make sure we're using the correct IATA (some flights do multiple trips between different locations)
                origin = flight.xpath('.//td[4]/a/text()').get().strip(' ()')
                destination = flight.xpath('.//td[5]/a/text()').get().strip(' ()')
                flight_status['origin'] = origin
                flight_status['destination'] = destination
            except (ValueError, AttributeError) as e:
                timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
                self.reportError(
                    timestamp,
                    response.url,
                    'recoverable',
                    'Flightradar table might be missing some information on origin/destination airports. Skipping this flight record...'
                )
            info = flight.xpath('.//td/text()').getall()
            # strip out characters
            for i in range(len(info)):
                info[i] = info[i].strip(' -')
                if self.debug:
                    print(f'{i}: {info[i]}')
            flight_date = info[1].split(' ')
            month = self.month_name.index(flight_date[1])
            flight_date = date(
                year=int(flight_date[2]),
                month=month,
                day=int(flight_date[0])
            )
            # figure out how the rows are set up
            # as they change depending on if the flight has departed/arrived
            if 'AM' in info[8] or 'PM' in info[8]:
                scheduled_departure = info[8]
                actual_departure = None
                scheduled_arrival = info[10]
                status = info[11]
            elif 'AM' in info[9] or 'PM' in info[9]:
                scheduled_departure = info[9]
                actual_departure = info[10]
                scheduled_arrival = info[11]
                status = info[12]
            try:
                # add times that are missing
                if 'arrival_time' not in flight_status:
                    # add the scheduled arrival time
                    # departure time should be in Perth timezone
                    # convert 12 hour time to 24 hour time
                    arrival = self.time_to_24(scheduled_arrival)
                    # convert time to UTC using updated timezone information
                    flight_status['arrival_time'] = time_to_utc(
                        flight_status['destination'],
                        datetime(
                            year=flight_date.year,
                            month=flight_date.month,
                            day=flight_date.day,
                            hour=int(arrival[0]),
                            minute=int(arrival[1])
                        )
                    )
                    # reset departure time to correct datetime
                    departure = self.time_to_24(scheduled_departure)
                    flight_status['departure_time'] = time_to_utc(
                        'PER',
                        datetime(
                            year=flight_date.year,
                            month=flight_date.month,
                            day=flight_date.day,
                            hour=int(departure[0]),
                            minute=int(departure[1])
                        )
                    )
                elif 'departure_time' not in flight_status:
                    # add the scheduled departure time
                    # arrival time should be in Perth timezone
                    # convert 12 hour time to 24 hour time
                    departure = self.time_to_24(scheduled_departure)
                    # convert time to UTC using updated timezone information
                    flight_status['departure_time'] = time_to_utc(
                        flight_status['origin'],
                        datetime(
                            year=flight_date.year,
                            month=flight_date.month,
                            day=flight_date.day,
                            hour=int(departure[0]),
                            minute=int(departure[1])
                        )
                    )
                    # reset arrival time to correct datetime
                    arrival = self.time_to_24(scheduled_arrival)
                    flight_status['arrival_time'] = time_to_utc(
                        'PER',
                        datetime(
                            year=flight_date.year,
                            month=flight_date.month,
                            day=flight_date.day,
                            hour=int(arrival[0]),
                            minute=int(arrival[1])
                        ))
                # set actual departure time information
                if actual_departure is not None and ('AM' in actual_departure or 'PM' in actual_departure):
                    # add actual departure time if it exists (flight has happened)
                    # convert 12 hour to 24 hour
                    actual_departure = self.time_to_24(actual_departure)
                    flight_status['actual_departure'] = 'Actual departure: ' + datetime(
                        year=flight_date.year,
                        month=flight_date.month,
                        day=flight_date.day,
                        hour=int(actual_departure[0]),
                        minute=int(actual_departure[1])
                    ).isoformat(' ')
                else:
                    # if the actual departure column does not have a time it is either scheduled or cancelled, use the status
                    actual_departure = status
                    flight_status['actual_departure'] = actual_departure
            except (ValueError, UnboundLocalError, KeyError, UnknownTimeZoneError) as e:
                timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
                self.reportError(
                    timestamp,
                    response.url,
                    'recoverable',
                    'Flightradar table might be missing some information. Continuing...'
                )

            flight_status['status'] = status
            # only insert data if its a perth scraped flight or between allowed regional airports
            if (
                ('per' in flight_status['origin_link'].lower()) or
                ((flight_status['destination'] in self.allowed_regional_airports) and (flight_status['origin'] in self.allowed_regional_airports))
            ):
                # insert airport iata into database
                destination_airport = AirportData(iata=flight_status['destination'])
                origin_airport = AirportData(iata=flight_status['origin'])
                self.mw.insert_airport(
                    destination_airport.iata,
                    destination_airport.latitude,
                    destination_airport.longitude,
                    destination_airport.name,
                    'UTC'
                )
                self.mw.insert_airport(
                    origin_airport.iata,
                    origin_airport.latitude,
                    origin_airport.longitude,
                    origin_airport.name,
                    'UTC'
                )
                # insert flight info into flight table
                self.mw.insert_flight(
                    flight_status['flight_number'],
                    flight_status['destination'],
                    flight_status['origin'],
                    flight_status['departure_time'],
                    flight_status['arrival_time']
                )
                # insert aircraft into db if an aircraft ID is given, if not set to unknown
                if flight_status['aircraft'] is not None and flight_status['aircraft'] != '':
                    self.mw.insert_aircraft(aircraft_id=flight_status['aircraft'])
                    aircraft = {
                        'aircraft_id': flight_status['aircraft'],
                        'aircraft_make': 'unknown',
                        'aircraft_model': 'unknown'
                    }
                else:
                    aircraft = {
                        'aircraft_id': 'unknown',
                        'aircraft_make': 'unknown',
                        'aircraft_model': 'unknown'
                    }
                # insert airline into db if airline name is given, if not set to unknown
                if flight_status['airline'] is not None:
                    self.mw.insert_airline(flight_status['airline'])
                    airline = flight_status['airline']
                else:
                    airline = 'unknown'
                # insert flightinfo with set aircraft and airline info
                self.mw.insert_flightinfo(
                    {
                        'flight_number': flight_status['flight_number'],
                        'departure_time': flight_status['departure_time'],
                        'arrival_time': flight_status['arrival_time']
                    },
                    aircraft,
                    airline
                )
                # insert flightstatus using set departure and arrival statuses
                # actual_departure key is either the same as the status (cancelled) or contains the departure time
                # status key always holds arrival status
                self.mw.insert_flightstatus(
                    {
                        'flight_number': flight_status['flight_number'],
                        'departure_time': flight_status['departure_time'],
                        'arrival_time': flight_status['arrival_time']
                    },
                    datetime.utcnow(),
                    flight_status['actual_departure'],
                    flight_status['status']
                )
                timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
                self.logScraping(
                    timestamp,
                    response.url[:299],
                    flight_status['flight_number'],
                    'flight status',
                    flight_status['departure_time'],
                    flight_status['arrival_time'],
                    status='success',
                    message='Scraped from Perth airport flightradar page'
                )
                yield(flight_status)

    def closed(self, reason):
        self.onSessionEnd()
        self.mw.close_connection()

    def time_to_24(self, time):
        twelve_hour = time.strip()[len(time) - 2:]
        time = time.strip(' AMPM').split(':')
        if 'pm' in twelve_hour.lower() and int(time[0]) < 12:
            time[0] = int(time[0]) + 12
        elif 'am' in twelve_hour.lower() and int(time[0]) == 12:
            time[0] = 0
        return time
