-- perth-flightradar-script.lua - Lua script to load more information on flightradar perth airport
--                                arrival and departure pages
-- Author - Jack Lardner
-- Last modified - 06/08/20
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html
-- Not designed to be run by itself, used by PerthAirportSpider in perth_airport.py

function main(splash, args)
    -- set splash with correct user agent and viewport for screenshots
    splash:set_user_agent(args.user_agent)
    splash:set_viewport_size(1920, 1080)
    -- go to url and wait to load
    assert(splash:go(args.url))
    assert(splash:wait(args.wait))

    -- click load more button and wait
    button = splash:select('.btn.btn-table-action.btn-flights-load')
    button:mouse_click()
    assert(splash:wait(args.wait))

    -- return rendered html and png
    return {
        html = splash:html(),
        png = splash:png()
    }
end