# flightradar_manager.py - Scraper manager that handles calling the Flightradar-Perth scraper
# Author - Jack Lardner
# Last modified - 21/10/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from datetime import datetime
from crochet import run_in_reactor


class FlightradarManager(Manager):
    routes = {
        'metro': [
            'filler-filler'
        ],
        'regional': [
            'filler-filler'
        ]
    }
    start_date = datetime(year=2020, month=9, day=2)

    def __init__(self):
        super().__init__('perth-airport', get_project_settings(), 'flightradar-start')

    def start(self, dates, route):
        print(f'scraping perth-airport')
        try:
            self.run('filler', 'filler', None).wait(9999)
        except Exception as e:
            print(e)

    @run_in_reactor
    def run(self, dep, dest, date):
        # none of the imports actually do anything, just to remain consistant
        runner = CrawlerRunner(self.scraper_settings)
        eventual = runner.crawl(self.scraper_name)
        return eventual
