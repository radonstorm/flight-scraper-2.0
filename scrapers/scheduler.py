# scheduler.py - Responsible for running many scrapers at a specified time
# Author - Jack Lardner
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from datetime import datetime, timedelta
import os
import pytz
import random
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from skippers.skippers_manager import SkippersManager
from airnorth.airnorth_manager import AirnorthManager
from google_flights.googleflights_manager import GoogleFlightsManager
from virgin.virgin_manager import VirginManager
from general.pushbullet import send_notification
import scheduler_config
import time
import sys
from crochet import setup, run_in_reactor
setup()

# import settings
# unique worker ID
worker_id = scheduler_config.WORKER_ID
days_ahead = scheduler_config.DAYS_AHEAD
num_workers = scheduler_config.NUM_WORKERS

managers = [
    SkippersManager(),
    # AirnorthManager(),
    VirginManager(),
    GoogleFlightsManager()
]


def restart_managers():
    """ Restart the scraper managers to ensure middleware objects are garbage collected
    """
    managers = [
        SkippersManager(),
        # AirnorthManager(),
        VirginManager(),
        GoogleFlightsManager()
    ]


def date_splits(worker_id, start_date, days_ahead=days_ahead, num_workers=num_workers, method='oneway'):
    """Splits the dates to scrape among scraper workers using unique ids
    days_ahead and num_workers must be evenly divisible
    """
    if days_ahead % num_workers != 0:
        raise ValueError('days_ahead and num_workers are not evenly divisible')
    dates = calculate_dates(start_date, days_ahead, method=method)
    workload = list()
    dates_per_worker = int(days_ahead / num_workers)
    for i in range(0, dates_per_worker):
        idx = worker_id * dates_per_worker + i
        if idx <= len(dates) - 1:
            workload.append(dates[idx])
    return workload


def calculate_dates(start_date, days_ahead=days_ahead, method='oneway'):
    """Calculates what dates to scrape for
    days_ahead is how many days in the future you want to scrape for ie: today + 60 days
    """
    # start_date = datetime(2020, 10, 2)
    # max date is the furthest possible date to scrape for
    max_date = datetime.now() + timedelta(days=days_ahead)
    # cycle date is the closest possible date to scrape for
    cycle_date = start_date + timedelta(days=days_ahead)
    dates = []
    # if the cycle date is still in the future
    if (max_date - cycle_date).days < days_ahead:
        limit = (max_date - cycle_date).days + 1
    # else we have passed the initial cycle date, use today as the closest possible date
    else:
        limit = days_ahead
        cycle_date = datetime.now()
    # loop through and compile a list of dates that can plug straight into qantas scraper
    if method == 'oneway' or method == 'fixed':
        for i in range(0, limit):
            day = cycle_date + timedelta(days=i)
            padded_day = str(day.day)
            padded_month = str(day.month)
            # make sure single digit dates have leading zeros
            if len(padded_day) == 1:
                padded_day = '0' + padded_day
            if len(padded_month) == 1:
                padded_month = '0' + padded_month
            dates.append(','.join((str(day.year), padded_month, padded_day)))
    if method == 'interval':
        for i in range(0, days_ahead):
            departure = start_date + timedelta(days=i)
            returndate = max_date - timedelta(days=i)
            if departure < returndate:
                dates.append((departure, returndate))
    if method == 'fixed':
        newdates = []
        today = datetime.today()
        for date in dates:
            newdates.append((today, date))
        dates = newdates
    return dates


# method to get the times from database
# modified to just read from a dict
def get_times(date):
    mw = Flightdata_Middleware()
    utctime = pytz.utc.localize(datetime.utcnow())
    times_str = {
        'metro': mw.get_setting('metro_times', utctime).split(','),
        'regional': mw.get_setting('regional_times', utctime).split(',')
    }
    if 'now' in sys.argv:
        # to ease starting the script during testing
        mode = None
        if 'regional' in sys.argv:
            mode = 'regional'
        elif 'metro' in sys.argv:
            mode = 'metro'
        times_str[mode] = []
        testing_time = datetime.now() + timedelta(minutes=1)
        times_str[mode].append(str(testing_time.hour) + ':' + str(testing_time.minute))
    times = {
        'metro': [],
        'regional': []
    }
    # read each time, add date info and place in time queue
    for time in times_str['metro']:
        time = time.split(':')
        time = datetime(date.year, date.month, date.day, int(time[0]), int(time[1]))
        times['metro'].append(time)
    for time in times_str['regional']:
        time = time.split(':')
        time = datetime(date.year, date.month, date.day, int(time[0]), int(time[1]))
        times['regional'].append(time)
    mw.close_connection()
    return times


def start():
    # list of times the scheduler will run the managers
    # assume the scheduler is to be ran for the current date
    time_queue = get_times(datetime.now().date())
    print('starting scheduler')
    try:
        # loop indefinitely
        while True:
            # check the next time for both metro or regional times, will be used to sleep for that amount
            if not time_queue['regional'] and time_queue['metro']:
                next_time = time_queue['metro'][0]
            elif not time_queue['metro'] and time_queue['regional']:
                next_time = time_queue['regional'][0]
            elif time_queue['metro'] and time_queue['regional']:
                next_time = min(time_queue['metro'][0], time_queue['regional'][0])

            # if there are metro times left in queue and it is time to start scraping (within a minute of the specified time)
            if time_queue['metro'] and time_queue['metro'][0] <= datetime.now() and datetime.now() <= (time_queue['metro'][0] + timedelta(minutes=1)):
                total_time_started = datetime.now()
                for manager in managers:
                    print(f'\nstarting {manager.scraper_name} scraper')
                    time_started = datetime.now()
                    for route in manager.routes['metro']:
                        manager.start(date_splits(worker_id, manager.start_date), route)
                    time_finished = datetime.now()
                    print(f'Time elapsed: {time_finished - time_started}')
                print(f'Total time elapsed: {datetime.now() - total_time_started}')
                restart_managers()
                time_queue['metro'].pop(0)
            # if there are regional times left in queue and it is time to start scraping (within a minute of the specified time)
            elif time_queue['regional'] and time_queue['regional'][0] <= datetime.now() and datetime.now() <= (time_queue['regional'][0] + timedelta(minutes=1)):
                total_time_started = datetime.now()
                for manager in managers:
                    print(f'\nstarting {manager.scraper_name} scraper')
                    time_started = datetime.now()
                    for route in manager.routes['regional']:
                        manager.start(date_splits(worker_id, manager.start_date), route)
                    time_finished = datetime.now()
                    print(f'Time elapsed: {time_finished - time_started}')
                print(f'Total time elapsed: {datetime.now() - total_time_started}')
                restart_managers()
                time_queue['regional'].pop(0)
            # if both time queues are empty then we must have run through all of the times, load the queue up with times for tomorrow
            elif not time_queue['metro'] and not time_queue['regional']:
                # check here if settings have changed
                time_queue = get_times((datetime.now() + timedelta(days=1)).date())
            # else it isn't time yet, so wait until the correct time
            else:
                # strip out seconds and milliseconds as we don't want them to influence our comparisons
                now = datetime.now()
                now = datetime(now.year, now.month, now.day, now.hour, now.minute, 0, 0)
                next_time = datetime(next_time.year, next_time.month, next_time.day, next_time.hour, next_time.minute, 0, 0)
                td = next_time - now
                # ignore times that have passed already today by removing from queue
                # this will happen if the scheduler is started after the first time to scrape
                if not td.total_seconds() >= 0:
                    print('Current time: {0}'.format(now))
                    print('Next scheduled scrape: {0}'.format(next_time))
                    print('Time {0} has already passed, skipping...'.format(next_time))
                    if time_queue['metro'][0] == next_time:
                        time_queue['metro'].pop(0)
                    elif time_queue['regional'][0] == next_time:
                        time_queue['regional'].pop(0)
                # else it isn't time to run scrapers yet, so wait until it is the correct time
                else:
                    print('Current time: {0}\nNext scheduled scrape: {1}'.format(now, next_time))
                    print('Sleeping for {0} seconds'.format(td.total_seconds()))
                    time.sleep(td.total_seconds())
    # user stops scheduler
    except KeyboardInterrupt:
        print('\nUser has stopped the script')
    except NotImplementedError as e:
        print('\nScraper manager has not implemented a run method')
        print(e)
    except Exception as e:
        print('\nSomething went wrong')
        print(e)
        hostname = os.popen('hostname').read().strip()
        send_notification(f'{hostname} has crashed')
        raise e
    finally:
        print('Exiting...')


if __name__ == '__main__':
    start()
