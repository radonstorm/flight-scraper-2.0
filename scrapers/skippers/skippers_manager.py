# skippers_manager.py - Scraper manager that handles calling the Skippers scraper
# Author - Jack Lardner
# Last modified - 13/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from datetime import datetime
from .skippers.spiders.skippers_sel import SkippersSelSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from crochet import run_in_reactor
import time


class SkippersManager(Manager):
    # routes for this scraper need to be in IATA format
    # must be in departure-destination order
    routes = {
        'metro': [
            # empty, skippers doesn't cover metro routes
        ],
        'regional': [
            'BME-FIZ',
            'BME-HCQ',
            'FIZ-BME',
            'FIZ-HCQ',
            'HCQ-BME',
            'HCQ-FIZ',
            'LVO-PER',
            'LVO-LNO',
            'LNO-PER',
            'MKR-PER',
            'MKR-MMG',
            'MMG-PER',
            'PER-MKR',
            'PER-MMG',
            'PER-LVO',
            'PER-LNO',
            'PER-WUN',
            'WUN-PER',
            'WUN-MKR',
            'WUN-MMG'
        ]
    }

    def __init__(self):
        super().__init__('skippers_sel', get_project_settings(), 'skippers_start')
        self.wait_time = 5

    def start(self, dates, route):
        # need to override the start function to not reverse the routes because its not guaranteed
        print(f'scraping {route}')
        for date in dates:
            try:
                self.run(route.split('-')[0], route.split('-')[1], date).wait(9999)
                time.sleep(self.wait_time)
            except Exception as e:
                print(e)

    @run_in_reactor
    def run(self, dep, dest, date):
        # ensure the args passed are valid for the scraper
        print(f'scraping {date} {dep}-{dest}')
        date = date.split(',')
        date = date[2] + ',' + date[1]
        runner = CrawlerRunner(self.scraper_settings)
        eventual = runner.crawl(SkippersSelSpider, dep=dep, dest=dest, date=date)
        return eventual
