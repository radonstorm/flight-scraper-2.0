# skippers_sel.py - Spider for Skippers Airline written using Selenium
# replaces old Splash based scraper
# Author - Jack Lardner
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl skippers_sel -a dep=IATA -a dest=IATA -a date=DD,MM

import scrapy
import os
import time
import platform
import calendar
from datetime import datetime, timedelta
from pathlib import Path
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from scrapy.selector import Selector
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import time_to_utc, time_to_24
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class SkippersSelSpider(LoggingSpider):
    name = 'skippers_sel'
    allowed_domains = ['skippers.com.au']
    start_urls = ['https://www.skippers.com.au/']
    link = 'https://booking.skippers.com.au/widget/searchelement.php?action=avl,fnd,ock&lang=en&direction=horizontal'

    def __init__(self, dep, dest, date, webdriver_path=None, debug=False, *args, **kwargs):
        super(SkippersSelSpider, self).__init__(*args, **kwargs)
        if debug is not False:
            debug = True
        self.debug = debug
        default_webdriver_name = 'chromedriver'
        if platform.system() == 'Windows':
            default_webdriver_name += '.exe'
        self.webdriver_path = Path(__file__).resolve().parents[3] / default_webdriver_name
        if webdriver_path is not None:
            self.webdriver_path = webdriver_path
        self.webdriver_options = Options()
        self.webdriver_options.add_argument('window_size=1920,1080')
        if not debug:
            self.webdriver_options.add_argument('--disable-gpu')
            self.webdriver_options.add_argument('--lang=en_US')
        self.dep = AirportData(iata=dep)
        self.dest = AirportData(iata=dest)
        self.months = list(calendar.month_name)
        self.date = date.split(',')
        self.month_name = self.months[int(self.date[1])].lower()

    def start_requests(self):
        yield scrapy.Request(
            self.link,
            callback=self.start_navigation
        )

    def start_navigation(self, response):
        browser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options)
        browser.set_window_size(1920, 1080)
        browser.get(self.link)
        time.sleep(2)
        # click oneway button
        browser.find_element_by_id('bagBtn').click()
        time.sleep(2)
        dep_field = browser.find_element_by_id('flightFrom')
        dest_field = browser.find_element_by_id('flightTo')
        date_field = browser.find_element_by_id('flightDepart')
        ActionChains(browser).click(dep_field).send_keys(self.dep.iata).pause(0.3).send_keys(Keys.ARROW_DOWN, Keys.ENTER).perform()
        time.sleep(2)
        ActionChains(browser).click(dest_field).send_keys(self.dest.iata).pause(0.3).send_keys(Keys.ARROW_DOWN, Keys.ENTER).perform()
        time.sleep(2)
        date_field.click()
        time.sleep(1)
        # find correct month first
        # will only check the 12 pages (one year ahead), if not found then quit
        found = False
        i = 0
        while not found and i <= 12:
            for element in browser.find_elements_by_xpath('//*/div[@class="dateFrame_title"]'):
                self.year = element.find_element_by_xpath('.//span[2]').text.lower()
                if element.find_element_by_xpath('.//span[1]').text.lower() == self.month_name:
                    found = True
                    break
            if not found:
                browser.find_element_by_class_name('flightForm_dateNextMonth').click()
                i += 1
                time.sleep(2)
        # click correct date
        travel_date = browser.find_element_by_xpath(f'//*/a[@data-monthnumber="{self.date[1]}" and @data-daynumber="{str(int(self.date[0]))}"]')
        # ensure date is clickable
        if 'disabled' not in travel_date.get_attribute('class'):
            travel_date.click()
            time.sleep(0.5)
            browser.find_element_by_id('avl').click()
            try:
                WebDriverWait(browser, 10).until(
                    EC.presence_of_all_elements_located((By.CSS_SELECTOR, '#outboundFlightListContainer'))
                )
                time.sleep(2)
                flight_expand = browser.find_elements_by_class_name('flightItem_titleBtn')
                for element in flight_expand:
                    element.click()
                # loaded all flight results, bundle the HTML and send it off for parsing
                time.sleep(2)
                if self.debug:
                    filename = f'{self.date[0]}-{self.date[1]} {self.dep.iata}-{self.dest.iata}.png'
                    browser.save_screenshot(os.path.join(os.path.dirname(__file__), filename))
                html = Selector(text=browser.page_source)
                # parse the results and yield them
                results = self.parse(html)
                for result in results:
                    yield(result)
            except TimeoutException:
                timestamp = datetime.utcnow()
                print('No flights')
                self.reportError(
                    timestamp,
                    self.link,
                    'error',
                    'No flights found'
                )
                self.logScraping(
                    timestamp,
                    self.link,
                    'ERROR',
                    'airfare',
                    '2030-01-01 00:00:00',
                    '2030-01-01 01:00:00',
                    status='error',
                    message='No flights found'
                )
        # date isn't clickable, no flights available
        else:
            timestamp = datetime.utcnow()
            print('No flights, date not available')
            self.reportError(
                timestamp,
                self.link,
                'error',
                'No flights found'
            )
            self.logScraping(
                timestamp,
                self.link,
                'ERROR',
                'airfare',
                '2030-01-01 00:00:00',
                '2030-01-01 01:00:00',
                status='error',
                message='No flights found'
            )
        browser.quit()
        self.onSessionEnd()

    def parse(self, html):
        flights = []
        flight_data = html.css('.flightItem.flightHolder')
        middleware = Flightdata_Middleware()
        for flight in flight_data:
            if flight.css('.flightChoice_tooltipToggle::text').get() == 'Non stop':
                flight_number = flight.xpath('.//*[@class="flightItem_titleRight"]/strong/text()').get().split(' ')[1].strip()
                departure_html = flight.xpath('.//*[@class="flightItem_titleTime"]')[0]
                arrival_html = flight.xpath('.//*[@class="flightItem_titleTime"]')[1]

                # strip out all time info
                departure_time = departure_html.css('strong::text').get().strip()
                departure_date = departure_html.css('span::text').get().split('|')[0].strip()
                departure = datetime.strptime(departure_date + f' {self.year} ' + departure_time, '%d %b %Y %H:%M')
                departure = time_to_utc(self.dep.iata, departure)

                arrival_time = arrival_html.css('strong::text').get().strip()
                arrival_date = arrival_html.css('span::text').get().split('|')[0].strip()
                arrival = datetime.strptime(arrival_date + f' {self.year} ' + arrival_time, '%d %b %Y %H:%M')
                arrival = time_to_utc(self.dest.iata, arrival)

                # grab all class html
                classes_html = flight.css('.flight-class__box')
                fares = {}
                for fare_html in classes_html:
                    class_name = fare_html.css('.flight-class__heading--primary::text').get().replace('Fare', '').upper()
                    fare = float(fare_html.xpath('.//*/a[@class="btn-class"]/span[2]/text()').get())
                    fares[class_name] = fare
                flight_info = {
                    'flight_number': flight_number,
                    'departure_iata': self.dep.iata,
                    'destination_iata': self.dest.iata,
                    'departure_time': departure,
                    'arrival_time': arrival,
                    'fares': fares
                }
                flights.append(flight_info)
                time_now = datetime.utcnow()

                middleware.insert_flight(
                    flight_info['flight_number'],
                    flight_info['destination_iata'],
                    flight_info['departure_iata'],
                    flight_info['departure_time'],
                    flight_info['arrival_time']
                )

                middleware.insert_flightinfo(
                    flight_info,
                    {
                        'aircraft_id': 'unknown',
                        'aircraft_make': 'unknown',
                        'aircraft_model': 'unknown'
                    },
                    'Skippers'
                )

                for fare_class in flight_info['fares']:
                    middleware.insert_airfare(
                        flight_info['flight_number'],
                        flight_info['departure_time'],
                        flight_info['arrival_time'],
                        time_now,
                        fare_class,
                        flight_info['fares'][fare_class],
                        'one way'
                    )

                self.logScraping(
                    time_now,
                    self.link,
                    flight_info['flight_number'],
                    'airfare',
                    flight_info['departure_time'],
                    flight_info['arrival_time'],
                    'success',
                    'Scraped Skippers airfare'
                )
            else:
                print('flight had stops, skipping...')
        middleware.close_connection()
        return flights
