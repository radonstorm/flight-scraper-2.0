# skippers.py
# IMPORTANT: THIS SCRAPER NO LONGER WORKS AND IS NO LONGER MAINTAINED
# author : Ivandy Darmawan, Jack Lardner
# Scraper for Flight-Scraper project 2019 Sem 2
# This scraper will scrape the flight detail of Skippers airline for one-way flight
# To run scraper, use this command:
# scrapy crawl skippers -a dep=<Departure Airport IATA> -a dest=<Destination Airport IATA> -a travel_date=<DD>,<MM>
# To capture screenshot add '-a debug=True'

import base64
import calendar
from scrapy_splash import SplashRequest
from datetime import timedelta, datetime, date
from time import strptime
import os.path
from scrapy.exceptions import CloseSpider

# python_packages
from general.logging_spider import LoggingSpider
from general.user_agents import user_agents
from general.airport_data import AirportData
from general.timezones import time_to_utc
from general.timezones import http_header_to_utc
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class InvalidArgError(Exception):
    pass


class SkippersSpider(LoggingSpider):
    name = 'skippers'
    link = r'https://www.skippers.com.au/book-a-flight/'

    # taking the lua script from a file
    fn = os.path.join(os.path.dirname(__file__), "skippers.lua")
    file = open(fn)
    script = file.read()
    file.close()

    auth = 'Basic ' + base64.encodebytes(b'user:userpass').decode('utf-8').strip()

    airport_iata = {
        'BME': 'Broome',
        'FIZ': 'Fitzroy Crossing',
        'HCQ': 'Halls Creek',
        'LVO': 'Laverton',
        'LNO': 'Leonora',
        'MKR': 'Meekatharra',
        'MMG': 'Mt Magnet',
        'PER': 'Perth',
        'WUN': 'Wiluna'
    }

    def __init__(self, dep=None, dest=None, travel_date=None, user_agent='chrome', debug=False, *args, **kwargs):
        super(SkippersSpider, self).__init__(*args, **kwargs)
        print('==================\n\nTHIS SCRAPER IS NO LONGER MAINTAINED\n\n==================\n')
        self.argsErr = False
        self.debug = debug
        self.user_agent = user_agents[user_agent]

        try:
            if travel_date is None:
                print(f'\n======= travel_date is not specified use -a travel_date=<DD>,<MM>\n')
                raise InvalidArgError()
            else:
                self.travel_day = int(travel_date.split(',')[0])
                self.travel_month_name = calendar.month_name[int(travel_date.split(',')[1])]
                self.travel_month = int(travel_date.split(',')[1])
            # if no departure and destination
            if dep is None and dest is None:
                print(f'\n======= {dep} and {dest} are not specified use -a dep=<Dep IATA> -a dest=>Dest IATA\n')
                raise InvalidArgError()
            elif dep not in self.airport_iata:
                self.argsErr = True
                print(f'\n======= {dep} is not available / not a valid airport\n')
                raise InvalidArgError()
            else:
                self.dep = AirportData(iata=dep)
                self.dest = AirportData(iata=dest)

        except InvalidArgError:
            self.argsErr = True

    def start_requests(self):
        if not self.argsErr:
            splash_args = {
                'lua_source': self.script,
                'html': 1,
                'images': 0,
                'dep': self.airport_iata[self.dep.iata],
                'dest': self.airport_iata[self.dest.iata],
                'day': self.travel_day,
                'month': self.travel_month_name,
                'detail': False,
                'timeout': 90
            }
            print('\n============= ' + f'scraping {self.dep.iata}-{self.dest.iata} on {self.travel_month_name} {self.travel_day}...\n')
            yield SplashRequest(
                self.link,
                endpoint='execute',
                callback=self.parse_result_page,
                cache_args=['lua_source'],
                args=splash_args,
                session_id="1",
                splash_headers={
                    'Authorization': self.auth
                }
            )

    def parse_result_page(self, response):
        if self.debug:
            print('Scraping result page')
            imgdata = base64.b64decode(response.data['png'])
            filename = 'skippers-' + self.dep.iata + '-' + self.dest.iata + '.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)
        if response.data["dest_unavailable"]:
            print("Destination is unavailable for given origin")
            raise CloseSpider("route unavailable")
        elif response.data["no_form"]:
            endUrl = response.data["url"]
            timestamp = datetime.datetime.utcnow()
            if response.data["cause"] == "blocked":
                self.logScraping(timestamp, endUrl, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Scraper has been blocked")
                self.reportError(timestamp, endUrl, "fatal", f"It appears that the scraper has been blocked. No further requests will be sent this run")
                raise CloseSpider("Blocked")
            else:
                self.logScraping(timestamp, endUrl, "ERROR", "airfare", "2030-01-01 00:00:00", "2030-01-01 01:00:00", "failed", "Unable to find form")
                self.reportError(timestamp, endUrl, "fatal", f"Could not find the form for unknown reason. Maybe the site format has changed? No further requests will be sent this run")
                raise CloseSpider("No Form")
        else:
            num_headings = response.xpath('//*[@class="text-pageheadings"]').getall()
            flight_data = response.xpath(r'//*[@id="tblOutFlightBlocks"]/tbody/tr[3]/td/text()').getall()
            flight_number = flight_data[0]
            departure_time = flight_data[2]
            arrival_time = flight_data[4]
            # prices
            price_skipp_e_flex = response.xpath(r'//*[@id="tblOutFlightBlocks"]/tbody/tr[3]/td[4]/label/text()').get()
            if price_skipp_e_flex is None:
                price_skipp_e_flex = 'Sold Out'
            price_skipp_e_semi_flex = response.xpath(r'//*[@id="tblOutFlightBlocks"]/tbody/tr[3]/td[5]/label/text()').get()
            if price_skipp_e_semi_flex is None:
                price_skipp_e_semi_flex = 'Sold Out'
            price_skipp_e_saver = response.xpath(r'//*[@id="tblOutFlightBlocks"]/tbody/tr[3]/td[6]/label/text()').get()
            if price_skipp_e_saver is None:
                price_skipp_e_saver = 'Sold Out'
            price_skipp_e_deal = response.xpath(r'//*[@id="tblOutFlightBlocks"]/tbody/tr[3]/td[7]/label/text()').get()
            if price_skipp_e_deal is None:
                price_skipp_e_deal = 'Sold Out'

            flight_detail = {
                'departure iata': self.dep.iata,
                'arrival iata': self.dest.iata,
                'flight number': flight_number,
                'aircraft': 'unknown',
                'departure time': departure_time,
                'arrival time': arrival_time,
                'SKIPP-E-FLEX': price_skipp_e_flex,
                'SKIPP-E-SEMI-FLEX': price_skipp_e_semi_flex,
                'SKIPP-E-SAVER': price_skipp_e_saver,
                'SKIPP-E-DEAL': price_skipp_e_deal
            }
            # add data to database if flight is available
            if len(num_headings) > 4:
                middleware = Flightdata_Middleware()
                time_now = datetime.utcnow()

                # convert departure_time and arrival_time into datetime objects
                if self.travel_day < time_now.day and self.travel_month < time_now.month:
                    year = time_now.year + 1
                else:
                    year = time_now.year

                # convert 12-hour into 24-hour
                dep_hour = int(departure_time.split(':')[0])
                if departure_time.split(':')[1][3:] == 'PM' and dep_hour != 12:
                    dep_hour = dep_hour + 12
                arr_hour = int(arrival_time.split(':')[0])
                if arrival_time.split(':')[1][3:] == 'PM' and arr_hour != 12:
                    arr_hour = arr_hour + 12

                dep_datetime = time_to_utc(
                    self.dep.iata, datetime(
                        year=year,
                        month=self.travel_month,
                        day=self.travel_day,
                        hour=dep_hour,
                        minute=int(departure_time.split(':')[1][:2])
                    )
                )

                arr_datetime = time_to_utc(
                    self.dest.iata, datetime(
                        year=year,
                        month=self.travel_month,
                        day=self.travel_day,
                        hour=arr_hour,
                        minute=int(arrival_time.split(':')[1][:2])
                    )
                )

                middleware.insert_flight(
                    flight_number,
                    self.dest.iata,
                    self.dep.iata,
                    dep_datetime,
                    arr_datetime
                )

                middleware.insert_flightinfo(
                    {
                        'flight_number': flight_number,
                        'departure_time': dep_datetime,
                        'arrival_time': arr_datetime
                    },
                    {
                        'aircraft_id': 'unknown',
                        'aircraft_make': 'unknown',
                        'aircraft_model': 'unknown'
                    },
                    'Skippers'
                )

                airfares = [
                    (flight_number, time_now, 'SKIPP-E-FLEX', 'One Way', price_skipp_e_flex.replace('$', '')),
                    (flight_number, time_now, 'SKIPP-E-SEMI-FLEX', 'One Way', price_skipp_e_semi_flex.replace('$', '')),
                    (flight_number, time_now, 'SKIPP-E-SAVER', 'One Way', price_skipp_e_saver.replace('$', '')),
                    (flight_number, time_now, 'SKIPP-E-DEAL', 'One Way', price_skipp_e_deal.replace('$', '')),
                ]
                for airfare in airfares:
                    if airfare[4].isdigit():
                        middleware.insert_airfare(
                            flight_number,
                            dep_datetime,
                            arr_datetime,
                            time_now,
                            airfare[2],
                            float(airfare[4]),
                            'one way'
                        )

                # log session
                timestamp = http_header_to_utc(response.headers['Date'].decode('utf-8'))
                self.logScraping(
                    timestamp,
                    self.link,
                    flight_number,
                    'airfare',
                    dep_datetime,
                    arr_datetime,
                    'success',
                    'Scraped Skippers airfare'
                )
                yield(flight_detail)
                middleware.close_connection()
                self.onSessionEnd()
            else:
                print('No flights on this day')
