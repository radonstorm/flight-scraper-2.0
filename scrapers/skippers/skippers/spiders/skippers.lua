-- skippers.lua
-- IMPORTANT: THIS SCRIPT NO LONGER WORKS AND IS NO LONGER MAINTAINED
-- author : Ivandy Darmawan
-- last modified : 4 Nov 2019
-- This is a lua script for skippers.py for the Flight-Scraper project
-- This script will select the origin, dest and flight date from the given args

function main(splash, args)
    assert(splash:go{
        args.url,
        headers=args.headers,
        http_method=args.http_method,
        body=args.body,
    })
    local form = splash:select("#container")
    if form == nil then
        local body = splash:select("body")
        local content = body:text()
        if string.find(content, "blocked") == nil then
            return {
                url = splash:url(),
                no_form=true,
                cause="unknown",
            }
        else
            return {
                url = splash:url(),
                no_form=true,
                cause="blocked",
            }
        end
    else
        --oneway trip
        assert(splash:wait(0.5))
        splash:select('input[value="One Way"]'):mouse_click()
        assert(splash:wait(0.5))

        splash:select('#from-origin'):send_text(args.dep)
        assert(splash:wait(0.5))
        
        splash:select('#departure-date'):mouse_click()
        assert(splash:wait(0.5))
        --[[
            There is a bug in the following JavaScript code where if you mean to specify
            a date for next year that shares the same month as the current month the function
            will result in unexpected behaviour. EG: Selecting a date intended for January 25 2021
            while it is January 2020, however selecting from February will work.
            It should not affect the running of the script for its intended purpose
            and as such won't be fixed
        ]]
        local select_date = splash:jsfunc([[
        function(day, month){
            var monthHeader = document.querySelector('span.ui-datepicker-month');
            while (!monthHeader.firstChild.data.includes(month)){
                document.querySelector("a[data-handler=next]").click();
                var monthHeader = document.querySelector('span.ui-datepicker-month');
            }
        document.querySelectorAll('.ui-state-default')[day-1].click()
        }
        ]])
        select_date(args.day, args.month)
        assert(splash:wait(0.5))
        if splash:select("#to-origin"):send_text(args.dest) == nil then
            return {
                --png = splash:png(), 
                --trust me it does not work, you dont need to see the screenshot
                url = splash:url(),
                no_form=false,
                dest_unavailable = true,
            }
        else 
            splash:select("#to-origin"):send_text(args.dest)
            splash:wait(0.5)
            splash:select('#submit-form'):mouse_click()
            assert(splash:wait(5))
            local entries = splash:history()
            local last_response = entries[#entries].response
            return {
                url = splash:url(),
                headers = last_response.headers,
                http_status = last_response.status,
                html = splash:html(),
                png = splash:png(),
                dest_unavailable = false,
                no_form=false,
            }
        end
    end
end