# virgin_manager.py - Scraper manager for virgin scraper
# Author - Jack Lardner
# Last modified - 4/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from datetime import datetime, timedelta
from .virgin.spiders.virgin_new import VirginNewSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from crochet import run_in_reactor


class VirginManager(Manager):
    routes = {
        'metro': [
            'PER-SYD',
            'PER-MEL',
            'PER-BNE',
            'PER-ADL',
            'PER-CNS',
            'BNE-SYD',
            'BNE-MEL'
        ],
        'regional': [
            'PER-KTA',
            'PER-KGI',
            'PER-PHE',
            'PER-ZNE',
            'PER-BME',
            'PER-KNX',
            'BME-KNX'
        ]
    }

    def __init__(self):
        super().__init__('virgin_new', get_project_settings(), 'virgin_start')
        self.wait_time = 1

    @run_in_reactor
    def run(self, dep, dest, date):
        print(f'scraping {date} {dep}-{dest}')
        runner = CrawlerRunner(self.scraper_settings)
        eventual = runner.crawl(VirginNewSpider, dep=dep, dest=dest, date=date)
        return eventual
