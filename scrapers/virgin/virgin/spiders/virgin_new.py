# virgin_new.py - New spider for virgin airfare data based on a Selenium scraper
# Author - Jack Lardner
# Last modified - 7/12/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

import scrapy
import time
import os
import platform
import logging
from scrapy.selector import Selector
from pathlib import Path
from datetime import datetime, timedelta
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.remote_connection import LOGGER as selenium_logger
from urllib3.connectionpool import log as urlliblogger
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import time_to_utc, time_to_24, str_to_dt
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class VirginNewSpider(LoggingSpider):
    name = 'virgin_new'
    start_urls = ['https://www.virginaustralia.com/au/']
    link = 'https://www.virginaustralia.com/au/en/bookings/flights/make-a-booking/'

    # CSS classes that correspond to fare prices
    fare_classes = {
        'Getaway': 'GW',
        'Elevate': 'EV',
        'Freedom': 'FD',
        'Business Saver': 'BS',
        'Business': 'BU'
    }

    def __init__(self, dep, dest, date, webdriver_path=None, debug=False, *args, **kwargs):
        super(VirginNewSpider, self).__init__(*args, **kwargs)
        selenium_logger.setLevel(logging.WARNING)
        urlliblogger.setLevel(logging.WARNING)
        if debug is not False:
            debug = True
        default_webdriver_name = 'chromedriver'
        if platform.system() == 'Windows':
            default_webdriver_name += '.exe'
        self.debug = debug
        self.webdriver_path = Path(__file__).resolve().parents[3] / default_webdriver_name
        if webdriver_path is not None:
            self.webdriver_path = webdriver_path
        self.webdriver_options = Options()
        self.webdriver_options.add_argument('window_size=1920,1080')
        if not debug:
            self.webdriver_options.add_argument('--disable-gpu')
            self.webdriver_options.add_argument('--lang=en_US')
        self.dep = AirportData(iata=dep)
        self.dest = AirportData(iata=dest)
        self.date = date.split(',')
        self.date = datetime(
            int(self.date[0]),
            int(self.date[1]),
            int(self.date[2])
        )

    def start_requests(self):
        yield scrapy.Request(
            self.link,
            callback=self.start_navigation
        )

    def start_navigation(self, response):
        browser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options)
        browser.set_window_size(1920, 1080)
        browser.get(self.link)
        WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '.flights-container'))
        )
        time.sleep(10)
        # click one way option
        browser.find_element_by_id('flights-oneway').click()
        # enter departure destination
        departure_field = browser.find_element_by_id('flights-originSurrogate')
        ActionChains(browser).click(departure_field).send_keys(self.dep.iata[:2]).pause(0.3).send_keys(self.dep.iata[2:]).perform()
        time.sleep(0.3)
        # enter arrival destination
        arrival_field = browser.find_element_by_id('flights-destinationSurrogate')
        ActionChains(browser).click(arrival_field).send_keys(self.dest.iata[:2]).pause(0.3).send_keys(self.dest.iata[2:]).pause(0.3).send_keys(Keys.TAB).perform()
        # browser.find_element_by_class_name('close').click()
        time.sleep(0.3)
        # remove COVID warning popup
        arrival_field.click()
        time.sleep(0.3)
        # insert departure date
        browser.find_element_by_id('flights-departure-date').click()
        time.sleep(0.3)
        month_dropdown = Select(browser.find_element_by_css_selector('.monthSelect.cform.left'))
        month_dropdown.select_by_visible_text(self.date.strftime('%B %Y'))
        days = browser.find_elements_by_css_selector('.ui-datepicker-group.ui-datepicker-group-first a')
        # calculate the correct date element to click
        if self.date.month == datetime.today().month and datetime.today().day > 1:
            idx = self.date.day - datetime.today().day + 1
        else:
            idx = self.date.day
        days[idx].click()
        # submit and wait for results
        browser.find_element_by_css_selector('.submit.btnLarge.btnFlight').click()
        try:
            WebDriverWait(browser, 15).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '.flc-table'))
            )
            time.sleep(1)
            # show all fare types
            all_fares = browser.find_element_by_xpath('//*[text()="View all Fare Options"]').click()
            time.sleep(2)
            # save page html as a selector
            html = Selector(text=browser.page_source)
            if self.debug:
                filename = f'{self.date.date()} {self.dep.iata}-{self.dest.iata}.png'
                browser.save_screenshot(os.path.join(os.path.dirname(__file__), filename))
            # crawl through each flight detail link
            flight_details = browser.find_elements_by_css_selector('.flight-number-and-direction a')
            details = []
            for detail_link in flight_details:
                time.sleep(0.7)
                # click the link
                detail_link.click()
                time.sleep(0.5)
                # save html of detail view as a selector
                detail = Selector(text=browser.page_source).xpath('//*/div[@class="modal-content clear-float"]')
                # use selector here to determine how many stops on the flight
                if len(detail.xpath('.//tbody/tr').getall()) > 1:
                    # break out of loop, the rest of the flights have stops
                    break
                if self.debug:
                    flight_num = detail.xpath('.//*/td/*/text()').getall()[4].strip().replace(' ', '').upper()
                    filename = f'{self.date.date()} {self.dep.iata}-{self.dest.iata} {flight_num} details.png'
                    browser.save_screenshot(os.path.join(os.path.dirname(__file__), filename))
                # add the detail selector to list and close detail view
                details.append(detail)
                browser.find_element_by_css_selector('.container-close').click()
            browser.quit()
            results = self.parse(html, details)
            for result in results.values():
                yield result
        except TimeoutException:
            print(f'timeout')
            timestamp = datetime.utcnow()
            self.reportError(
                timestamp,
                self.link,
                'error',
                'Timeout loading flight results'
            )
            self.logScraping(
                timestamp,
                self.link,
                'ERROR',
                'airfare',
                '2030-01-01 00:00:00',
                '2030-01-01 01:00:00',
                status='error',
                message='Timeout loading flight results'
            )
            browser.quit()
        self.onSessionEnd()

    def parse(self, html, flight_details):
        flights = {}
        # go through all details and populate the flights dictionary
        for flight in flight_details:
            details = flight.xpath('.//*/td/*/text()').getall()
            # only add flight to dictionary if it has no stops
            if len(details) == 7:
                num_stops = int(flight.xpath('(.//*/td)[4]/text()').get().strip())
                flight_number = details[4].strip().replace(' ', '').upper()
                origin = details[0].strip().upper()
                departure_time = details[1].strip()
                destination = details[2].strip().upper()
                arrival_time = details[3].strip()
                aircraft_model = details[5].strip()
                flights[flight_number] = {
                    'flight_number': flight_number,
                    'num_stops': num_stops,
                    'origin': origin,
                    'destination': destination,
                    'departure_time': departure_time,
                    'arrival_time': arrival_time,
                    'aircraft_model': aircraft_model
                }
        flight_html = html.xpath('//*/tr')
        airfares = {}
        for flight in flight_html:
            # scrape airfares
            flight_number = flight.xpath('.//*[@class="flight-number-and-direction"]/a/text()').get().strip().replace(' ', '').upper()
            collected_fares = {}
            for fare_class in self.fare_classes:
                css_class = self.fare_classes[fare_class]
                airfare = flight.xpath(f'.//*[@class="leadPriceOption {css_class} paddingb2c"]//*[@class="prices-amount"]/text()').get()
                if airfare is not None:
                    collected_fares[fare_class] = airfare.strip(' $,').replace(',', '')
            airfares[flight_number] = collected_fares
        # combine airfares with valid flights
        for flight in flights:
            flight_data = flights[flight]
            flight_data['airfare'] = airfares[flight]
            flights[flight] = flight_data
        # data validation
        time_now = datetime.utcnow()
        middleware = Flightdata_Middleware()
        for flight in flights.values():
            # convert departure time to UTC
            departure_time = time_to_24(flight['departure_time'])
            departure_time = datetime(self.date.year, self.date.month, self.date.day, int(departure_time[0]), int(departure_time[1]))
            departure_time = time_to_utc(self.dep.iata, departure_time)
            flight['departure_time'] = departure_time
            # convert arrival time to UTC
            arrival_time = time_to_24(flight['arrival_time'])
            arrival_time = datetime(self.date.year, self.date.month, self.date.day, int(arrival_time[0]), int(arrival_time[1]))
            arrival_time = time_to_utc(self.dest.iata, arrival_time)
            # handle overnight flights (if Virgin even runs overnighters)
            if arrival_time < departure_time:
                # need to convert back to a datetime then add 1 day then back to datetime
                # do not use time_to_utc, will convert to UTC again
                arrival_time = str_to_dt(arrival_time)
                arrival_time = arrival_time + timedelta(days=1)
                arrival_time = arrival_time.strftime('%Y-%m-%d %H:%M:%S %Z')
                arrival_time = arrival_time + 'UTC'
            flight['arrival_time'] = arrival_time
            # start inserting data
            timestamp = datetime.utcnow()
            middleware.insert_aircraft(
                aircraft_id='unknown',
                make='unknown',
                model=flight['aircraft_model']
            )
            middleware.insert_flight(
                flight['flight_number'],
                flight['destination'],
                flight['origin'],
                flight['departure_time'],
                flight['arrival_time']
            )
            middleware.insert_flightinfo(
                flight,
                {
                    'aircraft_id': 'unknown',
                    'aircraft_make': 'unknown',
                    'aircraft_model': flight['aircraft_model']
                },
                'Virgin'
            )
            for airfare in flight['airfare']:
                middleware.insert_airfare(
                    flight['flight_number'],
                    flight['departure_time'],
                    flight['arrival_time'],
                    time_now,
                    airfare,
                    flight['airfare'][airfare],
                    'one way'
                )
            self.logScraping(
                timestamp,
                self.link,
                flight['flight_number'],
                'airfare',
                flight['departure_time'],
                flight['arrival_time']
            )
        middleware.close_connection()
        return flights
