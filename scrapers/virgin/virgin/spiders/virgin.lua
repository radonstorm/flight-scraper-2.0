-- virgin.lua
-- author : Ivandy Darmawan
-- last modified : 27 Oct 2019
-- this script is used by virgin.py to run the splash
-- it will pass the information to search for flights in a given date

function main(splash, args)
    assert(splash:go{
        args.url,
        headers=args.headers,
        http_method=args.http_method,
        body=args.body,
      })
    splash:set_viewport_full()
    assert(splash:wait(0.5))
    local element = splash:select('#flights-oneway'):mouse_click()
    assert(splash:wait(0.5))
    element = splash:select('#flights-originSurrogate'):mouse_click()
    assert(splash:wait(0.5))
    local dep_field = splash:select('#flights-originSurrogate'):send_text(args.dep)
    assert(splash:wait(0.5))
    element = splash:select('#flights-destinationSurrogate'):mouse_click()
    assert(splash:wait(0.5))
    local dest_field = splash:select('#flights-destinationSurrogate'):send_text(args.dest)
    assert(splash:wait(0.5))
    for i=1,12 do local date_select = splash:select('#flights-departure-date'):send_keys('<Delete>') end
  	local date_select = splash:select('#flights-departure-date'):send_text(args.dates) --22 Dec, 2019
    assert(splash:wait(0.5))
    local sumbit = splash:select('.submit.btnLarge.btnFlight'):mouse_click()
    assert(splash:wait(15))
    
    return {
      html = splash:html(),
      url = splash:url(),
    }
  end