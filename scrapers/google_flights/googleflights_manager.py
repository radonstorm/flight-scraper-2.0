# googleflights_manager.py - Scraper manager for googleflights scraper
# Author - Jack Lardner
# Last modified - 19/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

import random
from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from datetime import datetime, timedelta
from .google_flights.spiders.googleflights import GoogleflightsSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from crochet import run_in_reactor


class GoogleFlightsManager(Manager):
    routes = {
        'metro': [
            'PER-SYD',
            'PER-MEL',
            'PER-BNE',
            'PER-ADL',
            'PER-CNS',
            'BNE-SYD',
            'BNE-MEL',
            'AKL-PER',
            'AKL-MEL',
            'AKL-SYD',
            'ALK-BNE',
            'OOL-PER',
            'OOL-SYD',
            'OOL-MEL',
            'OOL-BNE'
        ],
        'regional': [
            'PER-KTA',
            'KGI-PER',
            'PHE-PER',
            'ZNE-PER',
            'GET-PER',
            'BME-PER',
            'BME-DRW',
            'KNX-DRW',
            'KNX-PER',
            'LEA-PER',
            'BME-KNX',
            'BME-FIZ',
            'BME-HCQ',
            'BME-MEL',
            'BME-SYD',
            'LVO-PER',
            'LVO-LNO',
            'LNO-PER',
            'MKR-PER',
            'MKR-WUN',
            'MMG-PER',
            'MMG-MKR',
            'PER-WUN',
            'CVQ-PER',
            'EPR-PER',
            'ALH-PER',
            'MJK-CVQ',
            'ASP-PER',
            'BQB-PER',
            'CBR-PER',
            'CCK-PER',
            'HBA-PER',
            'ONS-KTA',
            'ONS-PER',
            'PER-PBO',
            'PBO-GET',
            'PER-XCH'
        ]
    }

    def __init__(self):
        super().__init__('googleflights', get_project_settings(), 'google_start')
        self.wait_time = 0.1

    @run_in_reactor
    def run(self, dep, dest, date):
        print(f'scraping {date} {dep}-{dest}')
        runner = CrawlerRunner(self.scraper_settings)
        eventual = runner.crawl(GoogleflightsSpider, dep=dep, dest=dest, date=date)
        return eventual


def runRandom():
    manager = GoogleFlightsManager()
    manager.scraper_settings.overrides['FEED_FORMAT'] = 'json'
    manager.scraper_settings.overrides['FEED_URI'] = 'data.json'
    scraped = set()
    while True:
        randomdelta = random.randint(0, 60)
        randomdate = manager.start_date + timedelta(days=randomdelta)
        randomtype = random.randint(0, 1)
        routetype = list(manager.routes.keys())[randomtype]
        randomroute = random.randint(0, len(manager.routes[routetype]) - 1)
        route = manager.routes[routetype][randomroute]
        date = [str(randomdate.year) + ',' + str(randomdate.month) + ',' + str(randomdate.day)]
        scrapehash = date[0] + route
        if scrapehash not in scraped:
            scraped.add(scrapehash)
            manager.start(date, route)


if __name__ == '__main__':
    runRandom()
