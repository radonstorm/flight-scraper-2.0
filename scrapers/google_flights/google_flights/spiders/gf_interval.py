# googleflights.py - Spider for googleflight airfare data
# Author - Jack Lardner
# Last modified - 25/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl googleflights -a dep=IATA -a dest=IATA -a date=YYYY-MM-DD

import scrapy
import os
import time
import platform
from datetime import datetime, timedelta
from pathlib import Path
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from scrapy.selector import Selector
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import time_to_utc, time_to_24
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class GfIntervalSpider(LoggingSpider):
    name = 'gf_interval'
    allowed_domains = ['google.com/flights']
    start_urls = ['http://google.com/flights/']
    link = 'http://google.com/flights'

    def __init__(self, dep, dest, dep_date, ret_date, webdriver_path=None, debug=False, *args, **kwargs):
        super(GfIntervalSpider, self).__init__(*args, **kwargs)
        if debug is not False:
            debug = True
        self.debug = debug
        default_webdriver_name = 'chromedriver'
        if platform.system() == 'Windows':
            default_webdriver_name += '.exe'
        self.webdriver_path = Path(__file__).resolve().parents[3] / default_webdriver_name
        if webdriver_path is not None:
            self.webdriver_path = webdriver_path
        self.webdriver_options = Options()
        self.webdriver_options.add_argument('window_size=1920,1080')
        if not debug:
            self.webdriver_options.add_argument('--disable-gpu')
            self.webdriver_options.add_argument('--lang=en_US')
        self.dep = AirportData(iata=dep)
        self.dest = AirportData(iata=dest)
        self.dep_date = dep_date.replace(',', '-')
        self.ret_date = ret_date.replace(',', '-')
        self.args = {
            'flt': f'{self.dep.iata}.{self.dest.iata}.{dep_date}',
            'c': 'AUD',  # denotes currency,
            'e': '1',
            's': '0',
            'sd': '1',
            't': 'f',
            'tt': 'r'  # (o)ne-way or (r)eturn
        }

    def start_requests(self):
        yield scrapy.Request(
            self.link,
            callback=self.start_navigation
        )

    def start_navigation(self, response):
        browser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options)
        browser.set_window_size(1920, 1080)
        browser.get(self.link)
        # create search query string
        concat = self.link + '#flt=' + self.args['flt']
        for key in self.args:
            if key != 'flt':
                arg = key + ':' + self.args[key]
                concat = concat + ';' + arg
        print(concat)
        browser.get(concat)
        time.sleep(2)
        # enter the CORRECT date
        date_window_value = datetime(
            int(self.dep_date.split('-')[0]),
            int(self.dep_date.split('-')[1]),
            int(self.dep_date.split('-')[2])
        ).strftime('%d %b %Y')
        ret_date_window_value = datetime(
            int(self.ret_date.split('-')[0]),
            int(self.ret_date.split('-')[1]),
            int(self.ret_date.split('-')[2])
        ).strftime('%d %b %Y')
        date_window = browser.find_element_by_xpath('/html/body/c-wiz[2]/div/div[2]/div/c-wiz/div/c-wiz/div[2]/div[1]/div/div[2]/div[2]/div/div/div[1]/div/div/div[1]/div/div[1]/div/input')
        class_dropdown = browser.find_element_by_xpath('/html/body/c-wiz[2]/div/div[2]/div/c-wiz/div/c-wiz/div[2]/div[1]/div/div[1]/div[3]/div/div[1]/div[1]/div/button/div[2]')
        date_window.send_keys(date_window_value + Keys.ENTER)
        date_window.send_keys(date_window_value + Keys.ENTER)
        time.sleep(2)
        ret_date_window = browser.find_element_by_xpath('//*[@id="yDmH0d"]/c-wiz[2]/div/div[2]/div/c-wiz/div/c-wiz/div[2]/div[1]/div/div[2]/div[2]/div/div/div[1]/div/div/div[1]/div/div[2]/div/input')
        ret_date_window.send_keys(ret_date_window_value + Keys.ENTER)
        ret_date_window.send_keys(ret_date_window_value + Keys.ENTER)
        time.sleep(10)
        return
        try:
            WebDriverWait(browser, 1).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, '.xKbyce'))
            )
            # click all flight detail buttons
            browser.execute_script("""
            for (e of document.querySelectorAll('.xKbyce')){
                e.click()
            }
            """)
            time.sleep(1)
            if self.debug:
                filename = f'{self.date} {self.dep.iata}-{self.dest.iata}.png'
                browser.save_screenshot(os.path.join(os.path.dirname(__file__), filename))
            # capture economy flight info
            economy_html = Selector(text=browser.page_source)
            class_dropdown.click()
            # click through to each class type
            class_selections = browser.find_elements_by_css_selector('div.A8nfpe.yRXJAe.iWO5td .Akxp3.d0tCmb.Lxea9c li')
            class_selections[1].click()
            time.sleep(1)
            premium_economy = Selector(text=browser.page_source)
            class_dropdown.click()
            class_selections[2].click()
            time.sleep(1)
            business = Selector(text=browser.page_source)
            class_dropdown.click()
            class_selections[3].click()
            time.sleep(1)
            first_class = Selector(text=browser.page_source)
            data = (
                economy_html,
                premium_economy,
                business,
                first_class
            )
            browser.quit()
            # results = self.parse(data)
            # for result in results.values():
            #     yield(result)
        except TimeoutException:
            timestamp = datetime.utcnow()
            print('No flights')
            self.reportError(
                timestamp,
                self.link,
                'error',
                'No flights found'
            )
            self.logScraping(
                timestamp,
                self.link,
                'ERROR',
                'airfare',
                '2030-01-01 00:00:00',
                '2030-01-01 01:00:00',
                status='error',
                message='No flights found'
            )
            browser.quit()
        self.onSessionEnd()

    def parse(self, data):
        flights = {}
        # loop through each class html
        for html in data:
            flight_data = html.css('.mz0jqb.taHBqe')
            # dict of flight, where the key is flight number + dep/arr datetimes
            for flight in flight_data:
                # select data for each flight
                price_html = flight.xpath('.//*[@class="XWuBZb"]/div/div[2]/span/text()').get()
                price = None
                if price_html is not None:
                    price = price_html.strip('$AUD ').replace(',', '')
                times = flight.xpath('.//*[@class="CrAOse-hSRGPd CrAOse-hSRGPd-TGB85e-cOuCgd hide-focus-ring"]/text()').getall()
                # convert to 24 hour datetime
                departure_time = time_to_24(times[0])
                arrival_time = time_to_24(times[1])
                datelist = self.date.split('-')
                departure_time = datetime(int(datelist[0]), int(datelist[1]), int(datelist[2]), int(departure_time[0]), int(departure_time[1]))
                arrival_time = datetime(int(datelist[0]), int(datelist[1]), int(datelist[2]), int(arrival_time[0]), int(arrival_time[1]))
                # handle overnight flights
                if arrival_time < departure_time:
                    arrival_time = arrival_time + timedelta(days=1)
                # convert to UTC time
                departure_time = time_to_utc(self.dep.iata, departure_time)
                arrival_time = time_to_utc(self.dest.iata, arrival_time)
                flightinfo = flight.xpath('.//*[@class="Xsgmwe"]/text()').getall()
                flight_number = flight.xpath('.//*[@class="Xsgmwe QS0io"]/text()').get()
                flight_number = flight_number.strip().replace(' ', '').replace('\xa0', '')
                airline = flightinfo[0].capitalize() + ' (GF)'
                flightclass = flightinfo[1] + ' (GF)'
                aircraft = flightinfo[2].split(' ')
                aircraft_make = aircraft[0]
                aircraft_model = ' '.join(aircraft[1:])
                if price is not None:
                    key = flight_number + departure_time + arrival_time
                    # insert a new flight record
                    if key not in flights:
                        flights[key] = {
                            'flight_number': flight_number,
                            'departure_time': departure_time,
                            'origin_iata': self.dep.iata,
                            'arrival_time': arrival_time,
                            'destination_iata': self.dest.iata,
                            'airfare': {
                                flightclass: price
                            },
                            'aircraft': {
                                'make': aircraft_make,
                                'model': aircraft_model
                            },
                            'airline': airline
                        }
                    # record exists, just insert airfare data
                    else:
                        flights[key]['airfare'][flightclass] = price

        middleware = Flightdata_Middleware()
        time_now = datetime.utcnow()
        for flight in flights.values():
            timestamp = datetime.utcnow()
            middleware.insert_airline(flight['airline'], self.link)
            middleware.insert_aircraft(
                aircraft_id='unknown',
                make=flight['aircraft']['make'],
                model=flight['aircraft']['model']
            )
            middleware.insert_flight(
                flight['flight_number'],
                flight['destination_iata'],
                flight['origin_iata'],
                flight['departure_time'],
                flight['arrival_time']
            )
            middleware.insert_flightinfo(
                flight,
                {
                    'aircraft_id': 'unknown',
                    'aircraft_make': flight['aircraft']['make'],
                    'aircraft_model': flight['aircraft']['model']
                },
                flight['airline']
            )
            for airfare in flight['airfare']:
                middleware.insert_airfare(
                    flight['flight_number'],
                    flight['departure_time'],
                    flight['arrival_time'],
                    time_now,
                    airfare,
                    flight['airfare'][airfare],
                    'one way'
                )
            self.logScraping(
                timestamp,
                self.link,
                flight['flight_number'],
                'airfare',
                flight['departure_time'],
                flight['arrival_time']
            )
        middleware.close_connection()
        return flights
