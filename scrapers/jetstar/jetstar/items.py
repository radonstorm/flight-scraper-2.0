# Define here the models for your scraped items
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class Flight(scrapy.Item):
    # Required fields for scraping; used in jetstar_selenium.py
    departureDate = scrapy.Field()
    depart_iata = scrapy.Field()
    arrival_iata = scrapy.Field()
    arrivalTime = scrapy.Field()
    departureTime = scrapy.Field()
    airline = scrapy.Field()
    flightNo = scrapy.Field()
    aircraftModel = scrapy.Field()
    airfare = scrapy.Field()
