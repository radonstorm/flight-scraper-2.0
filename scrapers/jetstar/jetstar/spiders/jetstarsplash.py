# jetstar.py - Spider for jetstar flight data from Google Flights
# Author - Dean Quaife
# Last modified - 11/01/2020
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl jetstar -a date=YYYY-MM-DD -a origin=origin_iata -a dest=destination_iata
# to output scraped flight data to a json format add -o flights.json
# to create a .png file of the page for testing purposes, add -a debug=true

import scrapy
import base64
from scrapy_splash import SplashRequest
import os.path
import pytz
import time
import datetime
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class JetstarSpider(scrapy.Spider):
    name = 'splash'
    url = 'https://www.google.com/flights'

    def __init__(self, date, origin, dest, debug=False, *args, **kwargs):
        self.date = date
        self.origin = origin
        self.dest = dest
        self.debug = debug
        # fetch lua script
        script_file = os.path.join(os.path.dirname(__file__), 'jetstar.lua')
        with open(script_file) as f:
            self.lua_script = f.read()

    # define splash arguments and call lua script
    def start_requests(self):
        splash_args = {
            'lua_source': self.lua_script,
            'url': self.url,
            'timeout': 90,
            'html': 1,
            'png': 1,
            'date': self.date,
            'origin': self.origin,
            'dest': self.dest
        }
        yield SplashRequest(self.url,
                            callback=self.parse,
                            endpoint='execute',
                            cache_args=['lua_source'],
                            args=splash_args)

    def parse(self, response):
        # add debug flag to take a screenshot
        if self.debug:
            imgdata = base64.b64decode(response.data['png'])
            filename = 'jetstar.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)

        # script has navigated to results page; retrieve data for each one
        i = 1
        for flight in response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li'):
            self.departureDate = response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/span[3]/text()').get()
            self.airfare = response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[1]/div[2]/div[1]/div[2]/div[3]/div/text()').get()
            self.departureTime = response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div[1]/span/span/text()').get()
            self.arrivalTime = response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[4]/div[1]/span/span/text()').get()
            self.airline = 'Jetstar'
            self.flightNo = 'JQ ' + response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/div[3]/span/span[2]/text()').get()
            self.aircraftModel = response.xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[7]/div[1]/div[5]/div[3]/div[1]/ol/li[' + str(i) + ']/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/div[3]/div[2]/span[1]/text()').get()
            i = i + 1
            yield {
                'departureDate': self.departureDate,
                'airfare': self.airfare,
                'origin': self.origin,
                'destination': self.dest,
                'departureTime': self.departureTime,
                'arrivalTime': self.arrivalTime,
                'airline': self.airline,
                'flightNo': self.flightNo,
                'aircraftModel': self.aircraftModel
            }
