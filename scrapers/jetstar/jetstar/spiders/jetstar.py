# jetstar.py - Spider for Jetstar airfare data
# Author - Jack Lardner
# Last modified - 7/12/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl jetstar dep=ORIGIN -a dest=DEST -a date=YYYY,MM,DD
# to output scraped flight data to a json format add -o flights.json
# only accounts for one way flights

import scrapy
import os
import platform
import time
import requests
import calendar
from datetime import datetime, timedelta
from pathlib import Path
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from scrapy.selector import Selector
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import time_to_utc, time_to_24
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from urllib.parse import urlencode


class JetstarSeleniumSpider(LoggingSpider):
    name = 'jetstar'
    start_urls = ['http://jetstar.com/au']
    link = 'https://www.jetstar.com/au/en/flights?'

    def __init__(self, dep, dest, date, webdriver_path=None, webdriver_options=None, manager=None, debug=False, *args, **kwargs):
        super(JetstarSeleniumSpider, self).__init__(*args, **kwargs)
        if debug is not False:
            debug = True
        self.debug = debug
        self.manager = manager
        default_webdriver_name = 'chromedriver'
        if platform.system() == 'Windows':
            default_webdriver_name += '.exe'
        self.webdriver_path = Path(__file__).resolve().parents[3] / default_webdriver_name
        if webdriver_path is not None:
            self.webdriver_path = webdriver_path
        if webdriver_options is None:
            self.webdriver_options = webdriver.ChromeOptions()
            self.webdriver_options.add_argument('window_size=1920,1080')
            # disable detection of webdriver
            self.webdriver_options.add_argument('--disable-blink-features=AutomationControlled')
            self.webdriver_options.add_experimental_option('prefs', {
                'profile.managed_default_content_settings.images': 2,
                'disk-cache-size': 4096
            })
            self.webdriver_capabilities = dict()
            if not debug:
                self.webdriver_options.add_argument('--disable-gpu')
                self.webdriver_options.add_argument('--lang=en_US')
        else:
            self.webdriver_options = webdriver_options['options']
            self.webdriver_capabilities = webdriver_options['capabilities']
            proxyinfo = requests.get('http://127.0.0.1:22999/api/proxies_running').json()
            for proxy in proxyinfo:
                if int(self.webdriver_capabilities['proxy']['httpProxy'].split(':')[2]) == proxy['port']:
                    print(f'resuming with proxy ip {proxy["ip"]}')
                    self.ipAddress = proxy['ip']
        self.dep = AirportData(iata=dep)
        self.dest = AirportData(iata=dest)
        self.date = date.replace(',', '-').split('-')
        self.months = list(calendar.month_name)
        # make sure date digits are padded with 0
        if len(self.date[1]) == 1:
            self.date[1] = '0' + self.date[1]
        if len(self.date[2]) == 1:
            self.date[2] = '0' + self.date[2]
        date_arg = f'{self.date[2]}-{self.date[1]}-{self.date[0]}'
        self.args = {
            'adults': '1',
            'children': '0',
            'destination': self.dest.iata,
            'flexible': '0',
            'flight-type': '1',
            'infants': '0',
            'origin': self.dep.iata,
            'selected-departure-date': date_arg
        }

    def start_requests(self):
        # load jetstar homepage, then submit link with query
        yield scrapy.Request(
            self.link,
            callback=self.navigation
        )

    def check_if_blocked(self, browser, link):
        blocked = True
        currentBrowser = browser
        while blocked:
            try:
                currentBrowser.get(link)
                time.sleep(0.3)
                currentBrowser.find_element_by_css_selector('.mps__row.submitFlightSearch button').click()
                WebDriverWait(currentBrowser, 7).until(
                    EC.presence_of_element_located((By.ID, 'sec-if-container'))
                )
                # get current proxy
                proxy = None
                if 'proxy' in self.webdriver_capabilities:
                    proxy = self.webdriver_capabilities['proxy']['httpProxy']
                # ask for next proxy given current port
                next_proxy_addr = self.next_proxy(proxy)
                print(f'setting to proxy address available on port {next_proxy_addr}')
                new_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
                if next_proxy_addr is not None:
                    new_proxy = Proxy({
                        'proxyType': ProxyType.MANUAL,
                        'httpProxy': next_proxy_addr,
                        'ftpProxy': next_proxy_addr,
                        'sslProxy': next_proxy_addr,
                        'noProxy': ''
                    })
                    new_proxy.add_to_capabilities(new_capabilities)
                    self.webdriver_capabilities = new_capabilities
                    # set proxy information for future scrapes
                    self.manager.set_proxy(proxy_options=self.webdriver_options, proxy_capabilities=new_capabilities)
                elif next_proxy_addr is None:
                    # disable proxy, set for future scrapes
                    self.manager.set_proxy()
                    print('disabling proxy')
                # create new browser with new capabilities
                currentBrowser.delete_all_cookies()
                currentBrowser.quit()
                currentBrowser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options, desired_capabilities=new_capabilities)
                currentBrowser.set_window_size(1920, 1080)
                time.sleep(1)
            except TimeoutException:
                print('Blocked message not found, continuing...')
                blocked = False
        return currentBrowser

    def navigation(self, response):
        browser = webdriver.Chrome(executable_path=self.webdriver_path, options=self.webdriver_options)
        browser.set_window_size(1920, 1080)
        self.link = self.link + urlencode(self.args)
        browser = self.check_if_blocked(browser, self.link)
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '.display-currency-AUD'))
            )
            browser.execute_script("""
            for (e of document.querySelectorAll('.info-show.js-more-info-button')){
                e.click()
            }
            """)
            time.sleep(1)
            browser.execute_script("""
            window.scrollBy(0, 10000)
            """)
            html = Selector(text=browser.page_source)
            browser.quit()
            results = self.parse(html)
            for result in results.values():
                yield result
        except TimeoutException:
            timestamp = datetime.utcnow()
            print('No flights found')
            self.reportError(
                timestamp,
                self.link,
                'error',
                'No flights found'
            )
            self.logScraping(
                timestamp,
                self.link,
                'ERROR',
                'airfare',
                '2030-01-01 00:00:00',
                '2030-01-01 01:00:00',
                status='error',
                message='No flights found'
            )
            browser.quit()
        self.onSessionEnd()

    def parse(self, html):
        flights = {}
        flights_html = html.css('.flight-card-wrapper')
        for flight in flights_html:
            num_stops = flight.xpath('.//div[@class="itinerary-info__icons"]/span').getall()
            if len(num_stops) != 1:
                break
            flight_number = flight.xpath('.//*[@class="medium-11 qa-flight-info-flightnumber-0"]/strong/text()').get()
            departure_time = flight.css('.depaturestation::text').get().strip().replace('\n', '')
            arrival_time = flight.css('.arrivalstation::text').get().strip().replace('\n', '')
            dep_date = departure_time.split(',')[0].strip().split(' ')
            dep_time = departure_time.split(',')[1].strip()
            arr_date = arrival_time.split(',')[0].strip().split(' ')
            arr_time = arrival_time.split(',')[1].strip()
            airfare = flight.xpath('.//*[@class="js-price pricepoint__middle"]/text()').get().strip(' $')
            aircraft = flight.css('.aircraftname::text').get().strip()
            aircraft_make = aircraft.split(' ')[0]
            aircraft_model = aircraft.split(' ')[1]
            # create proper time string and convert to 24-hour time
            if dep_time.find('am') != -1:
                dep_time = dep_time[:dep_time.find('am')] + ' AM'
            elif dep_time.find('pm') != -1:
                dep_time = dep_time[:dep_time.find('pm')] + ' PM'
            if arr_time.find('am') != -1:
                arr_time = arr_time[:arr_time.find('am')] + ' AM'
            elif arr_time.find('pm') != -1:
                arr_time = arr_time[:arr_time.find('pm')] + ' PM'
            dep_time = time_to_24(dep_time)
            arr_time = time_to_24(arr_time)
            # create departure datetime
            month = self.months.index(dep_date[2])
            departure_time = datetime(int(dep_date[3]), int(month), int(dep_date[1]), int(dep_time[0]), int(dep_time[1]))
            month = self.months.index(arr_date[2])
            arrival_time = datetime(int(arr_date[3]), int(month), int(arr_date[1]), int(arr_time[0]), int(arr_time[1]))
            data = {
                'flight_number': flight_number,
                'origin_iata': self.dep.iata,
                'destination_iata': self.dest.iata,
                'departure_time': time_to_utc(self.dep.iata, departure_time),
                'arrival_time': time_to_utc(self.dest.iata, arrival_time),
                'airfare': {
                    'Economy': airfare
                },
                'aircraft': {
                    'make': aircraft_make,
                    'model': aircraft_model
                },
                'airline': 'Jetstar'
            }
            flights[flight_number] = data
        print(f'num flights: {len(flights)}')
        middleware = Flightdata_Middleware()
        time_now = datetime.utcnow()
        for flight in flights.values():
            timestamp = datetime.utcnow()
            middleware.insert_aircraft(
                aircraft_id='unknown',
                make=flight['aircraft']['make'],
                model=flight['aircraft']['model']
            )
            middleware.insert_flight(
                flight['flight_number'],
                flight['destination_iata'],
                flight['origin_iata'],
                flight['departure_time'],
                flight['arrival_time']
            )
            middleware.insert_flightinfo(
                flight,
                {
                    'aircraft_id': 'unknown',
                    'aircraft_make': flight['aircraft']['make'],
                    'aircraft_model': flight['aircraft']['model']
                },
                flight['airline']
            )
            for airfare in flight['airfare']:
                middleware.insert_airfare(
                    flight['flight_number'],
                    flight['departure_time'],
                    flight['arrival_time'],
                    time_now,
                    airfare,
                    flight['airfare'][airfare],
                    'one way'
                )
            self.logScraping(
                timestamp,
                self.link,
                flight['flight_number'],
                'airfare',
                flight['departure_time'],
                flight['arrival_time']
            )
        middleware.close_connection()
        return flights
