function main(splash, args)
    --helper function
    function insert_text(field, text)
        assert(splash:wait(0.5))
        field:send_text(text)
        assert(splash:wait(1))
        field:send_keys('<Return>')
        assert(splash:wait(1))
    end

    --go to target website
    assert(splash:go(args.url))
    assert(splash:wait(5))

    --select one-way
    local element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div:nth-child(4) > div > div.gws-flights-form__form-card > div > div.gws-flights-form__secondary-constraints.gws-flights__flex-box > div:nth-child(1) > dropdown-menu > div > div.gws-flights__white-focus.gws-flights-form__menu-button')
    element:mouse_click()
    assert(splash:wait(0.5))
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div:nth-child(4) > div > div.gws-flights-form__form-card > div > div.gws-flights-form__secondary-constraints.gws-flights__flex-box > div:nth-child(1) > dropdown-menu > div > div.mSPnZKpnf91__menu.mSPnZKpnf91__cover-button.mSPnZKpnf91__open > menu-item:nth-child(2)')
    element:mouse_click()
    assert(splash:wait(1))

    --select origin
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div:nth-child(4) > div > div.gws-flights-form__form-card > div > div.gws-flights__flex-box.gws-flights__align-center > div.flt-input.gws-flights-form__input-container.gws-flights__flex-box.gws-flights-form__airport-input.gws-flights-form__swapper-right')
    element:mouse_click()
    assert(splash:wait(2))
    element = splash:select('#sb_ifc50')
    insert_text(element, args.origin)
    
    --select destination
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div:nth-child(4) > div > div.gws-flights-form__form-card > div > div.gws-flights__flex-box.gws-flights__align-center > div.flt-input.gws-flights-form__input-container.gws-flights__flex-box.gws-flights-form__airport-input.gws-flights-form__empty.gws-flights-form__swapper-left')
    element:mouse_click()
    assert(splash:wait(2))
    element = splash:select('#sb_ifc50')
    insert_text(element, args.dest)

    --select date and submit
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__home-page > div:nth-child(4) > div > div.gws-flights-form__form-card > div > div.gws-flights__flex-box.gws-flights__align-center > div.gws-flights-form__input-container.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__calendar-input.flt-body2 > div.flt-input.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__departure-input')
    element:mouse_click()
    assert(splash:wait(2))
    element = splash:select('#flt-modaldialog > div > div.xshS2d.gws-flights__flex-box > div.s8NXHf.gws-flights__flex-filler.gws-flights__flex-box > div.tj3PF.gws-flights__flex-filler.gws-flights__flex-box.i88iIb > date-input > input')
    insert_text(element, args.date)
    assert(splash:wait(2))
    element = splash:select('#flt-modaldialog > div > div.aFxfpc > g-raised-button')
    element:mouse_click()
    assert(splash:wait(5))

    --results page
    --select non-stop flights only
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__form.gws-flights__scrollbar-padding > div > div.gws-flights-form__form-card > div.gws-flights-form__filter-chips-container > div > div > filter-chip:nth-child(2)')
    element:mouse_click()
    assert(splash:wait(0.5))
    element = splash:select('#flt-modaldialog > div > div:nth-child(2) > div > ol > li:nth-child(2)')
    element:mouse_click()
    element = splash:select('#flt-modaldialog > div > div.gws-flights-filter__filter-dialog-header.flt-subhead1 > div.gws-flights-filter__filter-dialog-close')
    element:mouse_click()
    assert(splash:wait(0.5))

    --select jetstar flights only
    element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__form.gws-flights__scrollbar-padding > div > div.gws-flights-form__form-card > div.gws-flights-form__filter-chips-container > div > div > filter-chip:nth-child(3)')
    element:mouse_click()
    assert(splash:wait(1))
    element = splash:select('#flt-modaldialog > div > div.gws-flights-filter__airline-filter > div > div.gws-flights-filter__all-airlines-toggle > g-selection-control-switch > div > label')
    element:mouse_click()
    assert(splash:wait(2))

    --find jetstar in the list
    local i = 1
    local select_jetstar = splash:jsfunc([[
        function() {
            var i = 1;
            var listItem = document.querySelector('#flt-modaldialog > div > div.gws-flights-filter__airline-filter > div > div.flt-scrollable.gws-flights__dialog-scroll-region > section.gws-flights-filter__airline-list > ol > li:nth-child(1)');
            while (listItem == "Jetstar") {
                listItem = document.querySelector('#flt-modaldialog > div > div.gws-flights-filter__airline-filter > div > div.flt-scrollable.gws-flights__dialog-scroll-region > section.gws-flights-filter__airline-list > ol > li:nth-child('.concat(i, ')'));
                i = i + 1;
            }
            listItem.click()
        }
    ]])
    select_jetstar()
    element = splash:select('#flt-modaldialog > div > div.gws-flights-filter__filter-dialog-header.flt-subhead1 > div.gws-flights-filter__filter-dialog-close')
    element:mouse_click()
    assert(splash:wait(5))

    --open each results tab to allow main script to gather data using xpath
    splash:set_viewport_full()
    i = 1
    while(splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('.. i ..') > div > div.gws-flights-widgets-expandablecard__header.gws-flights-results__itinerary-card-header > div.gws-flights-results__itinerary-card-summary.gws-flights-results__result-item-summary.gws-flights__flex-box > div.gws-flights-results__expand > span') ~= nil)
    do
        element = splash:select('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('.. i ..') > div > div.gws-flights-widgets-expandablecard__header.gws-flights-results__itinerary-card-header > div.gws-flights-results__itinerary-card-summary.gws-flights-results__result-item-summary.gws-flights__flex-box > div.gws-flights-results__expand > span')
        element:mouse_click()
        i = i + 1
        assert(splash:wait(2))
    end
    assert(splash:wait(2))

    --screenshot and finish
    splash:set_viewport_full()
    return {
        png = splash:png(),
        html = splash:html(),
        cookies = splash:get_cookies()
    }
end