# jetstar_manager.py - Scraper manager for jetstar scraper
# Author - Jack Lardner
# Last modified - 9/12/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from general.manager import Manager
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from datetime import datetime, timedelta
from .jetstar.spiders.jetstar import JetstarSeleniumSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from crochet import run_in_reactor


class JetstarManager(Manager):
    routes = {
        'metro': [
            'PER-SYD',
            'PER-MEL',
            'PER-BNE',
            'PER-ADL',
            'PER-CNS',
            'BNE-SYD',
            'BNE-MEL'
        ],
        'regional': [
            # empty
        ]
    }

    def __init__(self):
        super().__init__('jetstar', get_project_settings(), 'jetstar_start')
        self.wait_time = 30
        self.proxy_expiry = None
        self.proxy_options = None

    @run_in_reactor
    def run(self, dep, dest, date):
        print(f'scraping {date} {dep}-{dest}')
        runner = CrawlerRunner(self.scraper_settings)
        if self.proxy_expiry is not None and self.proxy_expiry < datetime.now():
            self.proxy_options = None
            self.proxy_expiry = None
        eventual = runner.crawl(JetstarSeleniumSpider, dep=dep, dest=dest, date=date, webdriver_options=self.proxy_options, manager=self)
        return eventual

    def set_proxy(self, proxy_options=None, proxy_capabilities=None):
        """ Set the proxy for the current session
            enables persistent proxy connection
            By default resets proxy connection to None
        """
        self.proxy_options = {
            'options': proxy_options,
            'capabilities': proxy_capabilities
        }
        if proxy_options is None and proxy_capabilities is None:
            self.proxy_options = None
        if self.proxy_expiry is not None:
            self.proxy_expiry = datetime.now() + timedelta(seconds=500)
