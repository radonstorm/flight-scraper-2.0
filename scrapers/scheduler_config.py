# scheduler_config.py - Settings file for the scheduler.py script
# Author - Jack Lardner
# Last Modified - 05/11/20
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
import os

hostname = os.popen('hostname').read()
# worker id must be set for each scraper worker
WORKER_ID = int(hostname.split('-')[1]) - 1
DAYS_AHEAD = 60
NUM_WORKERS = 30
