# README
Welcome to the flight scrapers repo. Here you will find all code and documentation for the flight scrapers.

This project is separated into two parts: The scrapers themselves and the database that stores all scraped information.

The `scrapers` folder contains scrapers for each site, contained in its own project folder. The `scheduler.py` script is also contained here which is the main script used to run all scrapers at specified times.

The `database` folder contains SQL scripts used to generate the database structure and insert some foundational data the scrapers will assume is already inserted.

The `python_packages` folder is a library of supporting code that contains things like timezone converters, airport information, logging support and most importantly, middleware for adding data to the database.

The scrapers were built to collect data to support researchers at Curtin University.

## Installation
[Windows instructions](https://bitbucket.org/radonstorm/flight-scraper-2.0/wiki/windows_installation.md)

[Linux instructions](https://bitbucket.org/radonstorm/flight-scraper-2.0/wiki/Ubuntu%20Server%20Setup.md)

# Development Instructions
Below are instructions for developers
## Python Virtual Environment
1.  To setup your own python virtual environment type:  
    Linux:  
        `sudo apt-get install python3-venv    # If needed`  
        `python3 -m venv env`  

    Windows:  
        `python -m venv env`

2.  To activate the environment type:  
    Linux:
        `source env/bin/activate`

    Windows:
        `env/Scripts/Active`  

3.  Use pip to install required packages:  
        `pip install -r requirements.txt`  

4.  To deactivate the enironment type  
    Linux:
        `deactivate`

    Windows:    
        `env/Scripts/deactivate`  
    
NOTE: type "Set-ExecutionPolicy Unrestricted" in powershell if you are getting script execution restriction

### PYTHONPATH
Scrapers will require the `python_packages` folder to be added to your `$PYTHONPATH` environment variable

## Installation
1.  Setup postgresql database on own machine  
    see: http://www.postgresqltutorial.com/install-postgresql/ for installation instructions.

2.  In the `/python_packages/scraper_middleware` directory, edit a file called `database.ini`, changing as needed

## Splash
1.  Install Splash through Docker:

	```
	sudo docker pull scrapinghub/splash:3.3.1
	```


2.  Start Splash:

	```
	sudo docker run -it -p 8050:8050 scrapinghub/splash:3.3.1 --disable-private-mode
	```

	Port 8050 is used in this example, however any port can be used so long as it is updated in Scrapy's settings.py

	NOTE: ```--disable-private-mode``` flag must be used for some websites to render correctly

3.  Configure Scrapy

	Add the following to scrapy's settings.py:

	```
	SPLASH_URL = 'http://0.0.0.0:8050/'
	DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
	HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'
	```


	Add the following to SPIDER_MIDDLEWARES in settings.py

	```
	SPIDER_MIDDLEWARES = {
		'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
	}
	```


	Add the following to DOWNLOADER_MIDDLEWARES in settings.py

	```
	DOWNLOADER_MIDDLEWARES = {
		'scrapy_splash.SplashCookiesMiddleware': 723,
		'scrapy_splash.SplashMiddleware': 725,
		'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
	}
	```

## Linter
To run the linter on a specific file, run:
```
autopep8 -i <file name>
```

To run the linter on all files, run:
```
autopep8 -i -r ./
```

## Scrapers
Each scraper should be in a separate folder in the `./scrapers/` folder.

Any spiders should extend `LoggingSpider` from `general.logging_spider`, and use its methods to log information about each target scraped, and any errors that occurred.  
When the spider is finished (even if due to an error), it should call the `onSessionEnd` method of `LoggingSpider`.

## Middleware
To use the middleware users need to create a database ini file in this format
```ini
[postgresql]
host=localhost
database=flightdata
user=username
password=password
```
By default, the middleware will look for the file: `./python_packages/scraper_middleware/database.ini`, and will look under the section `postgresql`, but these can be set to other values using the `filename` and `section` keyword arguments to the middleware constructor.
