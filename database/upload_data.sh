#!/bin/bash
# upload_data.sh - A bash script to automate the upload of scraped data to a CloudStor folder
# This script assumes it has write permissions to the current directory and rclone is setup with a 'Cloudstor' entry
# This has been run successfully in a cron job by the 'postgres' user by copying the script to the user's home directory
# Author - Jack Lardner
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

# array containing the names of all database tables we want to dump
declare -a tables=("aircraft" "airfare" "airline" "airport" "flightinfo" "flights" "flightstatus" "scrapererrors" "scraperlogs" "scrapersessions" "scrapersettings")
cd /var/lib/postgresql
# dump the entire database data
pg_dump -O -x -d flight_scraper -f data.pgsql
echo "-- Generated on $(date)" | cat - data.pgsql > flight_scraper_data.pgsql
# dump each table in CSV format
for table in "${tables[@]}"
do
	psql -d flight_scraper -c "COPY ${table} TO '${PWD}/${table}TMP.csv' DELIMITER ',' CSV HEADER;"
	echo "# Generated on $(date)" | cat - "${table}TMP.csv" > "${table}.csv"
done
# dump the results of a SQL function that joins flights and flightstatus tables
psql -d flight_scraper -c "COPY (SELECT * FROM selectallflightstatus()) TO '${PWD}/allflightstatusqueryTMP.csv' DELIMITER ',' CSV HEADER;"
echo "# Generated on $(date)" | cat - allflightstatusqueryTMP.csv > allflightstatusquery.csv
# dump the results of a SQL function that joins flights and airfare tables
psql -d flight_scraper -c "COPY (SELECT * FROM selectallairfare()) TO '${PWD}/allairfarequeryTMP.csv' DELIMITER ',' CSV HEADER;"
echo "# Generated on $(date)" | cat - allairfarequeryTMP.csv > allairfarequery.csv
# dump the results of a SQL function that filters duplicate flightstatus records
psql -d flight_scraper -c "COPY (SELECT * FROM selectallflightstatusfiltered()) TO '${PWD}/allflightstatusqueryfilteredTMP.csv' DELIMITER ',' CSV HEADER;"
echo "# Generated on $(date)" | cat - allflightstatusqueryfilteredTMP.csv > allflightstatusqueryfiltered.csv
# dump the entire database using pg_dumpall as a database backup
timestamp=$(date +%F)
pg_dumpall > database_backupTMP.pgsql
echo "-- Generated on $(date)" | cat - database_backupTMP.pgsql > "$timestamp Backup.pgsql"
# send everything to the cloudstor folder
rclone --progress copy flight_scraper_data.pgsql Cloudstor:/Flight\ Scraper\ Data/
rclone --progress copy "$timestamp Backup.pgsql" Cloudstor:/Database\ Backup/
for table in "${tables[@]}"
do
	rclone --progress copy "${table}.csv" Cloudstor:/Flight\ Scraper\ Data/
done
rclone --progress copy allflightstatusquery.csv Cloudstor:/Flight\ Scraper\ Data/
rclone --progress copy allairfarequery.csv Cloudstor:/Flight\ Scraper\ Data/
rclone --progress copy allflightstatusqueryfiltered.csv Cloudstor:/Flight\ Scraper\ Data/
# cleanup
rm *.csv *.pgsql
