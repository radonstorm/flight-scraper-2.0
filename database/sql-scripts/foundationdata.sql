-- foundationdata.sql
-- used to insert preliminary data such as airlines, airports and common aircrafts
-- remember to update this if different websites are used for scraping...
-- Author - Jack Lardner
-- Last Modified - 05/11/20
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html

BEGIN;
-- INSERT airline data
INSERT INTO airline(name, website) VALUES('Error', 'www.used.for.errors.com.au');
INSERT INTO airline(name, website) VALUES('unknown', 'unknown');
INSERT INTO airline(name, website) VALUES('Qantas', 'https://www.qantas.com/au/en.html');
INSERT INTO airline(name, website) VALUES('AirNorth', 'https://secure.airnorth.com.au/AirnorthIBE/OnlineBooking.aspx');
INSERT INTO airline(name, website) VALUES('Jetstar', 'https://www.jetstar.com/au/en/');
INSERT INTO airline(name, website) VALUES('REX', 'https://www.rex.com.au/');
INSERT INTO airline(name, website) VALUES('Virgin', 'https://www.virginaustralia.com/au/en/bookings/flights/make-a-booking/');
INSERT INTO airline(name, website) VALUES('Skippers', 'https://www.skippers.com.au/book-a-flight/');

-- INSERT airport data
INSERT INTO airport VALUES('ERR', 0, 0, 'Used for errors', 'UTC');
INSERT INTO airport VALUES('PER', -31.94029998779297, 115.96700286865234, 'Perth International Airport', 'UTC');  
INSERT INTO airport VALUES('SYD', -33.94609832763672, 151.177001953125, 'Sydney Kingsford Smith International Airport', 'UTC');
INSERT INTO airport VALUES('MEL', -37.673302, 144.843002, 'Melbourne International Airport', 'UTC');
INSERT INTO airport VALUES('BNE', -27.384199142456055, 153.11700439453125, 'Brisbane International Airport', 'UTC');
INSERT INTO airport VALUES('ADL', -34.945, 138.531006, 'Adelaide International Airport', 'UTC');
INSERT INTO airport VALUES('CNS', -16.885799408, 145.755004883, 'Cairns Airport', 'UTC');
INSERT INTO airport VALUES('KTA', -20.712200164799995, 116.773002625, 'Karratha Airport', 'UTC');
INSERT INTO airport VALUES('KGI', -30.789400100699996, 121.461997986, 'Kalgoorlie Boulder Airport', 'UTC');
INSERT INTO airport VALUES('PHE', -20.3777999878, 118.625999451, 'Port Hedland International Airport', 'UTC');
INSERT INTO airport VALUES('ZNE', -23.417800903299998, 119.803001404, 'Newman Airport', 'UTC');
INSERT INTO airport VALUES('GET', -28.796101, 114.707001, 'Geraldton Airport', 'UTC');
INSERT INTO airport VALUES('BME', -17.9506085, 122.2314674, 'Broome International Airport', 'UTC');
INSERT INTO airport VALUES('KNX', -15.7781000137, 128.707992554, 'Kununurra Airport', 'UTC');
INSERT INTO airport VALUES('LEA', -22.235599517799997, 114.088996887, 'Learmonth Airport', 'UTC');
INSERT INTO airport VALUES('CVQ', -24.880211, 113.67174, 'Carnarvon Airport', 'UTC');
INSERT INTO airport VALUES('EPR', -33.684399, 121.822998, 'Esperance Airport', 'UTC');
INSERT INTO airport VALUES('ALH', -34.94329833984375, 117.80899810791016, 'Albany Airport', 'UTC');
INSERT INTO airport VALUES('MJK', -25.8938999176, 113.577003479, 'Shark Bay Airport', 'UTC');
INSERT INTO airport VALUES('MMG', -28.116100311279297, 117.84200286865234, 'Mount Magnet Airport', 'UTC');
INSERT INTO airport VALUES('MKR', -26.6117000579834, 118.5479965209961, 'Meekatharra Airport', 'UTC');

-- INSERT aircraft data
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('unknown', 'unknown', 0, 'unknown');
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('Airbus', 'A330', NULL, 'unknown');
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('Airbus', 'A380', NULL, 'unknown');
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('Airbus', 'A320', NULL, 'unknown');
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('Boeing', '737', NULL, 'unknown');
INSERT INTO aircraft (make, model, num_seats, aircraft_id) VALUES('Boeing', '777', NULL, 'unknown');

-- insert error flight for scraping errors
INSERT INTO flights (flight_number, destination_id, origin_id, departure_time, arrival_time) VALUES('ERROR', 'ERR', 'ERR', '2030-01-01 00:00:00', '2030-01-01 01:00:00');

-- insert settings
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('metro_times', '03:00,08:00,13:00,17:00,22:00', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('regional_times', '10:00', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('skippers_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('airnorth_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('virgin_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('google_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('qantas_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');
INSERT INTO scrapersettings (key, value, valid_from, valid_to) VALUES('jetstar_start', '2020-12-8', (SELECT NOW()), '2030-01-01 00:00:00+00');

-- add some useful functions to select data
CREATE FUNCTION SelectAllAirfare()
RETURNS TABLE(
    flight_number varchar(7),
    origin_id varchar(3),
    destination_id varchar(3),
    departure_time timestamp without time zone,
    arrival_time timestamp without time zone,
    time_collected timestamp without time zone,
    fare numeric(8,2),
    fare_class varchar(20),
    airline varchar(200)
) AS
$func$
BEGIN
    RETURN QUERY
    SELECT flights.flight_number,
    flights.origin_id,
    flights.destination_id,
    -- change UTC times to Perth time by adding 8 hours
    -- useful for researchers based in Perth
    (
        SELECT flights.departure_time + interval '8 hours'
    ) AS "departure_time",
    (
        SELECT flights.arrival_time + interval '8 hours'
    ) AS "arrival_time",
    (
        SELECT airfare.time_collected + interval '8 hours'
    ) AS "time_collected",
    airfare.fare,
    airfare.fare_class,
    (
        SELECT name FROM airline WHERE id = (
            SELECT airline_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "airline"
    FROM flights
    INNER JOIN airfare
    ON flights.id = airfare.flight_id;
END
$func$ LANGUAGE plpgsql;

CREATE FUNCTION SelectAllFlightStatus()
RETURNS TABLE(
    flight_number varchar(7),
    origin_id varchar(3),
    destination_id varchar(3),
    departure_time timestamp without time zone,
    arrival_time timestamp without time zone,
    time_collected timestamp without time zone,
    departure_status varchar(100),
    arrival_status varchar(100),
    airline varchar(200),
    aircraft_id varchar(10),
    aircraft_make varchar(200),
    aircraft_model varchar(200),
    aircraft_seats smallint
) AS
$func$
BEGIN
    RETURN QUERY
    SELECT flights.flight_number,
    flights.origin_id,
    flights.destination_id,
    -- change UTC times to Perth time by adding 8 hours
    -- useful for researchers based in Perth
    (
        SELECT flights.departure_time + interval '8 hours'
    ) AS "departure_time",
    (
        SELECT flights.arrival_time + interval '8 hours'
    ) AS "arrival_time",
    (
        SELECT flightstatus.time_collected + interval '8 hours'
    ) AS "time_collected",
    flightstatus.departure_status,
    flightstatus.arrival_status,
    (
        SELECT name FROM airline WHERE id = (
            SELECT airline_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "airline",
    (
        SELECT aircraft.aircraft_id FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_id",
    (
        SELECT aircraft.make FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_make",
    (
        SELECT aircraft.model FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_model",
    (
        SELECT aircraft.num_seats FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_seats"
    FROM flights
    INNER JOIN flightstatus
    ON flights.id = flightstatus.flight_id;
END
$func$ LANGUAGE plpgsql;

CREATE FUNCTION SelectAllFlightStatusFiltered()
RETURNS TABLE(
    flight_number varchar(7),
    origin_id varchar(3),
    destination_id varchar(3),
    departure_time timestamp without time zone,
    arrival_time timestamp without time zone,
    time_collected timestamp without time zone,
    departure_status varchar(100),
    arrival_status varchar(100),
    airline varchar(200),
    aircraft_id varchar(10),
    aircraft_make varchar(200),
    aircraft_model varchar(200),
    aircraft_seats smallint
) AS
$func$
BEGIN
    RETURN QUERY
    SELECT flights.flight_number,
    flights.origin_id,
    flights.destination_id,
    -- change UTC times to Perth time by adding 8 hours
    -- useful for researchers based in Perth
    (
        SELECT flights.departure_time + interval '8 hours'
    ) AS "departure_time",
    (
        SELECT flights.arrival_time + interval '8 hours'
    ) AS "arrival_time",
    (
        SELECT flightstatus.time_collected + interval '8 hours'
    ) AS "time_collected",
    flightstatus.departure_status,
    flightstatus.arrival_status,
    (
        SELECT name FROM airline WHERE id = (
            SELECT airline_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "airline",
    (
        SELECT aircraft.aircraft_id FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_id",
    (
        SELECT aircraft.make FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_make",
    (
        SELECT aircraft.model FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_model",
    (
        SELECT aircraft.num_seats FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_seats"
    FROM flights
    INNER JOIN flightstatus
    ON flights.id = flightstatus.flight_id
    WHERE (flights.departure_time - flightstatus.time_collected <= interval '48 hours') AND
    (flightstatus.time_collected - flights.arrival_time <= interval '24 hours');
END
$func$ LANGUAGE plpgsql;

CREATE FUNCTION NumAirfares(airline_name varchar(200))
RETURNS INTEGER AS $total$
DECLARE total INTEGER;
BEGIN
    SELECT COUNT(*) INTO total FROM SelectAllAirfare() WHERE time_collected > (SELECT NOW() - interval '24 hours') AND airline = airline_name AND fare_class NOT LIKE '%GF%';
    RETURN total;
END;
$total$ LANGUAGE plpgsql;

CREATE FUNCTION NumGoogleAirfares()
RETURNS INTEGER AS $total$
DECLARE total INTEGER;
BEGIN
    SELECT COUNT(*) INTO total FROM SelectAllAirfare() WHERE time_collected > (SELECT NOW() - interval '24 hours') AND fare_class LIKE '%GF%';
    RETURN total;
END;
$total$ LANGUAGE plpgsql;

CREATE FUNCTION Connections()
RETURNS INTEGER AS $total$
DECLARE
    total INTEGER;
BEGIN
    SELECT COUNT(state) INTO total FROM pg_stat_activity WHERE datname='flight_scraper';
    RETURN total;
END;
$total$ LANGUAGE plpgsql;

CREATE FUNCTION SelectAllFlightStatusForFlight(flight_num varchar(7))
RETURNS TABLE(
    flight_number varchar(7),
    destination_id varchar(3),
    origin_id varchar(3),
    departure_time timestamp without time zone,
    arrival_time timestamp without time zone,
    time_collected timestamp without time zone,
    departure_status varchar(100),
    arrival_status varchar(100),
    airline varchar(200),
    aircraft_id integer
) AS
$func$
BEGIN
    RETURN QUERY
    SELECT flights.flight_number,
    flights.destination_id,
    flights.origin_id,
    flights.departure_time,
    flights.arrival_time,
    flightstatus.time_collected,
    flightstatus.departure_status,
    flightstatus.arrival_status,
    (
        SELECT name FROM airline WHERE id = (
            SELECT airline_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "airline",
    (
        SELECT id FROM aircraft WHERE id = (
            SELECT flightinfo.aircraft_id FROM flightinfo WHERE flight_id = flights.id
        )
    ) AS "aircraft_id"
    FROM flights
    INNER JOIN flightstatus
    ON flights.id = flightstatus.flight_id
    WHERE flights.flight_number = flight_num;
END
$func$ LANGUAGE plpgsql;

-- procedure to create or modify a setting in the database
CREATE PROCEDURE CreateSetting(
    inkey varchar(200),
    invalue varchar(200)
)
LANGUAGE plpgsql
AS $$
BEGIN
    -- a current setting exists with the same key, change the valid_to field and insert the new setting
    IF EXISTS(SELECT * FROM scrapersettings WHERE key = inkey AND valid_to = '2030-01-01 00:00:00+00') THEN
        UPDATE scrapersettings SET valid_to = (SELECT NOW() AT TIME ZONE 'UTC') WHERE key = inkey AND valid_to = '2030-01-01 00:00:00+00';
    END IF;
    INSERT INTO scrapersettings(key, value, valid_from, valid_to) VALUES(
        inkey,
        invalue,
        (SELECT NOW() AT TIME ZONE 'UTC'),
        '2030-01-01 00:00:00+00'
    );
END
$$;

-- procedure to add number of seats to aircraft based on the FLIGHT NUMBER they serve
-- used to create a dataset for a postgrad student
CREATE PROCEDURE AddSeats(
    inseats smallint,
    inflight_number varchar(7)
)
LANGUAGE plpgsql
AS $$
BEGIN
    IF inflight_number IN (SELECT flight_number FROM flights) THEN
        UPDATE aircraft SET num_seats = inseats WHERE id IN (
            SELECT aircraft_id FROM flightinfo WHERE flight_id IN (
                SELECT id FROM flights WHERE flight_number = inflight_number
        ));
    END IF;
END
$$;

-- the following procedure is used to insert flightinfo into the production database
-- it ensures that aircraft information is updated where needed and created if not
CREATE PROCEDURE InsertFlightInfo(
    inflight_number varchar(7),
    indeparture_time timestamp without time zone,
    inarrival_time timestamp without time zone,
    inaircraft_id varchar(10),
    inaircraft_make varchar(200),
    inaircraft_model varchar(200),
    inairline_name varchar(200)
)
LANGUAGE plpgsql
AS $$
DECLARE
    selected_flight_info flightinfo%ROWTYPE;
    selected_aircraft aircraft%ROWTYPE;
    new_aircraft_id varchar(10);
    new_aircraft_make varchar(200);
    new_aircraft_model varchar(200);
    new_aircraft_seats smallint;
BEGIN
    -- select the FlightInfo that might already exist in the database
    SELECT * INTO selected_flight_info FROM flightinfo WHERE flightinfo.flight_id = (
        SELECT id FROM flights WHERE flights.flight_number = inflight_number AND
        flights.departure_time = indeparture_time AND
        flights.arrival_time = inarrival_time
    );
    IF EXISTS(SELECT * FROM flightinfo WHERE flightinfo.flight_id = selected_flight_info.flight_id) THEN
        -- flightinfo already exists, make sure to update accordingly
        -- RAISE NOTICE 'Found flightinfo %', selected_flight_info;
        SELECT * INTO selected_aircraft FROM aircraft WHERE aircraft.id = selected_flight_info.aircraft_id;
        -- RAISE NOTICE 'Has aircraft %', selected_aircraft;
        new_aircraft_id := selected_aircraft.aircraft_id;
        new_aircraft_make := selected_aircraft.make;
        new_aircraft_model := selected_aircraft.model;
        new_aircraft_seats := selected_aircraft.num_seats;
        -- run by qantas, given a known make and model
        IF inaircraft_make != 'unknown' AND inaircraft_model != 'unknown' THEN
            -- if aircraft id is in database assume make and model are not known
            IF selected_aircraft.aircraft_id != 'unknown'  AND selected_aircraft.make = 'unknown' AND selected_aircraft.model = 'unknown' THEN
                IF NOT EXISTS(SELECT * FROM aircraft WHERE aircraft.aircraft_id = selected_aircraft.aircraft_id AND aircraft.make = inaircraft_make AND aircraft.model = inaircraft_model) THEN
                    UPDATE aircraft SET
                        aircraft_id = selected_aircraft.aircraft_id,
                        make = inaircraft_make,
                        model = inaircraft_model
                    WHERE
                        aircraft_id = selected_aircraft.aircraft_id AND
                        make = selected_aircraft.make AND
                        model = selected_aircraft.model;
                END IF;
            -- else all aircraft data is unknown, insert new aircraft with make and model
            ELSIF selected_aircraft.aircraft_id = 'unknown' AND selected_aircraft.make = 'unknown' AND selected_aircraft.model = 'unknown' THEN
                INSERT INTO aircraft(aircraft_id, make, model, num_seats) VALUES(
                    selected_aircraft.aircraft_id,
                    inaircraft_make,
                    inaircraft_model,
                    selected_aircraft.num_seats
                ) ON CONFLICT(aircraft_id, make, model) DO NOTHING;
                UPDATE flightinfo SET
                    aircraft_id = (SELECT id FROM aircraft WHERE
                        aircraft.aircraft_id = selected_aircraft.aircraft_id AND
                        aircraft.make = inaircraft_make AND
                        aircraft.model = inaircraft_model
                    )
                WHERE flight_id = selected_flight_info.flight_id;
            END IF;
        -- run by flightradar, given a known aircraft id
        ELSIF inaircraft_id != 'unknown' THEN
            -- if aircraft id is not in database and make and model are known
            -- this should also cover when all aircraft data is unknown (as shown in the next ELSIF statement)
            IF selected_aircraft.aircraft_id = 'unknown' THEN
                -- insert new aircraft with new id and old make and model
                INSERT INTO aircraft(aircraft_id, make, model, num_seats) VALUES(
                    inaircraft_id,
                    selected_aircraft.make,
                    selected_aircraft.model,
                    selected_aircraft.num_seats
                ) ON CONFLICT(aircraft_id, make, model) DO NOTHING;
                UPDATE flightinfo SET
                    aircraft_id = (SELECT id FROM aircraft WHERE
                        aircraft.aircraft_id = inaircraft_id AND
                        aircraft.make = selected_aircraft.make AND
                        aircraft.model = selected_aircraft.model
                    )
                WHERE flight_id = selected_flight_info.flight_id;
            -- else all aircraft data is unknown, insert new aircraft with aircraft id
            -- ELSIF selected_aircraft.aircraft_id = 'unknown' AND selected_aircraft.make = 'unknown' AND selected_aircraft.model = 'unknown' THEN
            END IF;
        END IF;
    ELSE
        -- flightinfo not found, insert a new flightinfo
        -- RAISE NOTICE 'No Flightinfo found: %', selected_flight_info;
        INSERT INTO flightinfo(flight_id, aircraft_id, airline_id) VALUES(
            (SELECT id FROM flights WHERE
                flights.flight_number = inflight_number AND
                flights.departure_time = indeparture_time AND
                flights.arrival_time = inarrival_time
            ),
            (SELECT id FROM aircraft WHERE
                aircraft.aircraft_id = inaircraft_id AND
                aircraft.make = inaircraft_make AND
                aircraft.model = inaircraft_model
            ),
            (SELECT id FROM airline WHERE airline.name = inairline_name)
        );
    END IF;
END
$$;
COMMIT;