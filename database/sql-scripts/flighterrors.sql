-- flighterrors.sql
-- used to setup tables for recording logs and errors the flight scrapers encounter
-- initially generated from old Django models from when this was a student project
-- Author - Jack Lardner
-- Last Modified - 02/10/20
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html
-- TODO: ensure all foreign keys are given ON DELETE RESTRICT clauses when final

BEGIN;
--
-- Create model ScraperError
--
CREATE TABLE "scrapererrors" ("id" uuid NOT NULL PRIMARY KEY, "severity" varchar(1) NOT NULL, "error_details" varchar(200) NOT NULL);
--
-- Create model ScraperLog
--
CREATE TABLE "scraperlogs" ("id" serial NOT NULL PRIMARY KEY, "timestamp" timestamp with time zone NOT NULL, "target_url" varchar(300) NOT NULL, "data" varchar(1) NOT NULL, "message" varchar(300) NOT NULL, "status" varchar(1) NOT NULL);
--
-- Create model ScraperSession
--
CREATE TABLE "scrapersessions" ("id" serial NOT NULL PRIMARY KEY, "target_site" varchar(200) NOT NULL, "session_start" timestamp with time zone NOT NULL, "session_end" timestamp with time zone NOT NULL, "errors" varchar(1) NOT NULL, "scraper_ip" inet NOT NULL);
--
-- Create model ScraperSetting
--
CREATE TABLE "scrapersettings" ("id" serial NOT NULL PRIMARY KEY, "key" varchar(200) NOT NULL, "valid_from" timestamp with time zone NOT NULL, "value" varchar(200) NOT NULL, "valid_to" timestamp with time zone NOT NULL);
--
-- Create constraint from_before_to on model scrapersetting
--
ALTER TABLE "scrapersettings" ADD CONSTRAINT "from_before_to" CHECK ("valid_to" > ("valid_from"));
--
-- Alter unique_together for scrapersetting (1 constraint(s))
--
ALTER TABLE "scrapersettings" ADD CONSTRAINT "scrapersettings_key_valid_from_9ba28407_uniq" UNIQUE ("key", "valid_from");
--
-- Alter unique_together for scrapersession (1 constraint(s))
--
ALTER TABLE "scrapersessions" ADD CONSTRAINT "scrapersessions_target_site_session_start_f3270af6_uniq" UNIQUE ("target_site", "session_start");
--
-- Add field flight to scraperlog
--
ALTER TABLE "scraperlogs" ADD COLUMN "flight_id" integer NOT NULL;
--
-- Add field session to scraperlog
--
ALTER TABLE "scraperlogs" ADD COLUMN "session_id" integer NOT NULL;
--
-- Add field log to scrapererror
--
ALTER TABLE "scrapererrors" ADD COLUMN "log_id" integer NOT NULL;
--
-- Alter unique_together for scraperlog (1 constraint(s))
--
ALTER TABLE "scraperlogs" ADD CONSTRAINT "scraperlogs_timestamp_target_url_0447ad39_uniq" UNIQUE ("timestamp", "target_url");
CREATE INDEX "scraperlogs_flight_id_" ON "scraperlogs" ("flight_id");
ALTER TABLE "scraperlogs" ADD CONSTRAINT "scraperlogs_flight_id_fk_flights_id" FOREIGN KEY ("flight_id") REFERENCES "flights" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "scraperlogs_session_id_fe8f2a2a" ON "scraperlogs" ("session_id");
ALTER TABLE "scraperlogs" ADD CONSTRAINT "scraperlogs_session_id_fe8f2a2a_fk_scrapersessions_id" FOREIGN KEY ("session_id") REFERENCES "scrapersessions" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "scrapererrors_log_id_5a30916c" ON "scrapererrors" ("log_id");
ALTER TABLE "scrapererrors" ADD CONSTRAINT "scrapererrors_log_id_5a30916c_fk_scraperlogs_id" FOREIGN KEY ("log_id") REFERENCES "scraperlogs" ("id") DEFERRABLE INITIALLY DEFERRED;
COMMIT;
