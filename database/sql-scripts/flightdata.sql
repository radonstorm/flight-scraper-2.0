-- flightdata.sql
-- used to setup tables for recording flight data scraped from websites
-- initially generated from old Django models from when this was a student project
-- Author - Jack Lardner
-- Last Modified - 02/10/20
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html
-- TODO: ensure all foreign keys are given ON DELETE RESTRICT clauses when final

BEGIN;
--
-- Create Aircraft table
--
CREATE TABLE "aircraft" (
    "id" serial NOT NULL PRIMARY KEY,
    "aircraft_id" varchar(10) NOT NULL,
    "make" varchar(200) NOT NULL,
    "model" varchar(200) NOT NULL,
    "num_seats" smallint CHECK ("num_seats" >= 0),
    UNIQUE("aircraft_id", "make", "model")
);
--
-- Create Airline table
--
CREATE TABLE "airline" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(200) NOT NULL,
    "website" varchar(200),
    UNIQUE("name")
);
--
-- Create Airport table
--
CREATE TABLE "airport" (
    "iata_code" varchar(3) NOT NULL PRIMARY KEY,
    "latitude" numeric(7, 4),
    "longitude" numeric(7, 4),
    "name" varchar(100),
    "timezone" varchar(3)
);
--
-- Create Flights table
--
CREATE TABLE "flights" (
    "id" serial NOT NULL PRIMARY KEY,
    "flight_number" varchar(7) NOT NULL,
    "destination_id" varchar(3) NOT NULL,
    "origin_id" varchar(3) NOT NULL,
    "departure_time" timestamp NOT NULL,
    "arrival_time" timestamp NOT NULL,
    UNIQUE("flight_number", "departure_time", "arrival_time"),
    FOREIGN KEY("destination_id") REFERENCES airport("iata_code") ON DELETE RESTRICT,
    FOREIGN KEY("origin_id") REFERENCES airport("iata_code") ON DELETE RESTRICT
);
--
-- Create FlightInfo table
--
CREATE TABLE "flightinfo" (
    "flight_id" integer NOT NULL PRIMARY KEY,
    "aircraft_id" integer NOT NULL,
    "airline_id" integer NOT NULL,
    FOREIGN KEY("flight_id") REFERENCES flights("id") ON DELETE RESTRICT,
    FOREIGN KEY("aircraft_id") REFERENCES aircraft("id") ON DELETE RESTRICT,
    FOREIGN KEY("airline_id") REFERENCES airline("id") ON DELETE RESTRICT
);
--
-- Create FlightStatus table
--
CREATE TABLE "flightstatus" (
    "flight_id" integer NOT NULL,
    "time_collected" timestamp NOT NULL,
    "departure_status" varchar(100) NOT NULL,
    "arrival_status" varchar(100) NOT NULL,
    PRIMARY KEY("flight_id", "time_collected"),
    FOREIGN KEY("flight_id") REFERENCES flights("id") ON DELETE RESTRICT
);
--
-- Create TrackedRoute table
--
CREATE TABLE "trackedroutes" (
    "id" serial NOT NULL PRIMARY KEY,
    "start_tracking" timestamp NOT NULL,
    "stop_tracking" timestamp NOT NULL,
    "destination_id" varchar(3) NOT NULL,
    "origin_id" varchar(3) NOT NULL,
    UNIQUE("origin_id", "destination_id", "start_tracking"),
    FOREIGN KEY("destination_id") REFERENCES airport("iata_code" ) ON DELETE RESTRICT,
    FOREIGN KEY("origin_id") REFERENCES airport("iata_code") ON DELETE RESTRICT,
    CONSTRAINT "start_before_stop" CHECK("stop_tracking" > ("start_tracking"))
);
--
-- Create Airfare table
--
CREATE TABLE "airfare" (
    "flight_id" integer NOT NULL,
    "time_collected" timestamp NOT NULL,
    "fare_class" varchar(20) NOT NULL,
    "fare" numeric(8, 2),
    "fare_type" varchar(20),
    PRIMARY KEY("flight_id", "time_collected", "fare_class", "fare_type"),
    FOREIGN KEY("flight_id") REFERENCES flights("id") ON DELETE RESTRICT
);
-- the following is remaining Django generated SQL commands
CREATE INDEX "airline_name_like" ON "airline" ("name" varchar_pattern_ops);
CREATE INDEX "airport_iata_code_like" ON "airport" ("iata_code" varchar_pattern_ops);
CREATE INDEX "flights_flight_number_like" ON "flights" ("flight_number" varchar_pattern_ops);
CREATE INDEX "flights_destination_id" ON "flights" ("destination_id");
CREATE INDEX "flights_destination_id_like" ON "flights" ("destination_id" varchar_pattern_ops);
CREATE INDEX "flights_origin_id" ON "flights" ("origin_id");
CREATE INDEX "flights_origin_id_like" ON "flights" ("origin_id" varchar_pattern_ops);
CREATE INDEX "flightinfo_aircraft_model_id" ON "flightinfo" ("aircraft_id");
CREATE INDEX "flightinfo_airline_id" ON "flightinfo" ("airline_id");
CREATE INDEX "trackedroutes_destination_id" ON "trackedroutes" ("destination_id");
CREATE INDEX "trackedroutes_destination_id_like" ON "trackedroutes" ("destination_id" varchar_pattern_ops);
CREATE INDEX "trackedroutes_origin_id" ON "trackedroutes" ("origin_id");
CREATE INDEX "trackedroutes_origin_id_like" ON "trackedroutes" ("origin_id" varchar_pattern_ops);
CREATE INDEX "flightstatus_flight_id" ON "flightstatus" ("flight_id");
COMMIT;
